package com.mansouri.wholesalecarpart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener {

    Button btnLogin;
    EditText etUserName, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        initialize();
    }

    private void initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("ورود");
        Misc.setFont(this, tvTitleToolbar);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        etUserName = (EditText) findViewById(R.id.etMobile);
        etPassword = (EditText) findViewById(R.id.etLoginPassword);
        btnLogin.setOnClickListener(this);

        Misc.setFont(this, btnLogin, 18, true);
        Misc.setFont(this, etUserName, 25, false);
        Misc.setFont(this, etPassword, 25, false);

    }


    public void loginRequest(final String username, final String password) {
        String URL = Misc.Server_Url() + "login.php";
        Misc.showDialogProgress(ActivityLogin.this);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        startActivity(new Intent(ActivityLogin.this, ActivityHome.class));
                        Misc.dismissDialogProgress();
                        finish();
                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        Misc.setDialog(ActivityLogin.this, "خطا", "اطلاعات اشتباه", 'e');
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(ActivityLogin.this, "خطا", "اشکال در ارتباط با سرور", 'e');
//                startActivity(new Intent(ActivityLogin.this, ActivityHome.class));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }

        };
        RetryPolicy policy = new DefaultRetryPolicy(5 * 1000, 2, 2);
        sr.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(sr);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                loginRequest(etUserName.getText().toString(), etPassword.getText().toString());
//                finish();
                break;
        }
    }
}
