package com.mansouri.wholesalecarpart.adaptor;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.R;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.library.PersianDate;
import com.mansouri.wholesalecarpart.model.Order;

import java.util.List;


public class AdapterOrder extends BaseAdapter {

    private static LayoutInflater inflater = null;
    DATASource datasource;
    private Activity activity;
    private List<Order> data;

    public AdapterOrder(Activity a, List<Order> d) {
        activity = a;
        data = d;
        datasource = new DATASource(activity);

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.row_order, null);
        }

        TextView tvOrderId = (TextView) vi.findViewById(R.id.tvRowOrderId);
        TextView tvOrderer = (TextView) vi.findViewById(R.id.tvRowOrder);
        TextView tvOrderTime = (TextView) vi.findViewById(R.id.tvRowOrderTime);

        Misc.setFont(activity, tvOrderer, 16, true);
        Misc.setFont(activity, tvOrderTime, 10, false);
        Misc.setFont(activity, tvOrderId, 13, true);

        Order order = data.get(position);


        tvOrderId.setText("شماره سفارش: " + String.valueOf(order.getId()));
//        long time = System.currentTimeMillis();
//        Log.i("xxx time", String.valueOf(time));
//        Log.i("xxx order time", String.valueOf(order.getOrder_time()));

        tvOrderTime.setText("تاریخ سفارش: " + PersianDate.Shamsi(order.getOrder_time() * 1000));
        tvOrderer.setText(" سفارش از شرکت " + datasource.getCompanyName(order.getUser_id()));

        return vi;
    }

}