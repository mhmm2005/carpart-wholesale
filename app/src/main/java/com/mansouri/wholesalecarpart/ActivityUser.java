package com.mansouri.wholesalecarpart;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.adaptor.AdapterUser;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivityUser extends AppCompatActivity implements View.OnClickListener, AdapterUser.ComponentClickListener {

    FloatingActionButton btnActiveUser, btnDeletedUser, btnCreateNewUser;
    ListView list;
    DATASource datasource;
    AdapterUser adapterUser;
    TextView tvTitleToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);


        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapterUser = new AdapterUser(this, datasource.getAllUsers(1), 1);
        list.setAdapter(adapterUser);
    }

    private void initialize() {
        adapterUser = new AdapterUser();
        adapterUser.setOnComponentItemClickListener(this);

        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("مشتری های فعال");
        Misc.setFont(this, tvTitleToolbar);

        btnCreateNewUser = (FloatingActionButton) findViewById(R.id.btnFabCreateNewUser);
        btnDeletedUser = (FloatingActionButton) findViewById(R.id.btnFabShowDeactiveUser);
        btnActiveUser = (FloatingActionButton) findViewById(R.id.btnFabShowActiveUser);

        btnCreateNewUser.setOnClickListener(this);
        btnDeletedUser.setOnClickListener(this);
        btnActiveUser.setOnClickListener(this);
        list = (ListView) findViewById(R.id.listViewUser);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tvId = (TextView) view.findViewById(R.id.tvRowUserId);
                Log.i("xxx name", tvId.getText().toString());
                Intent intent = new Intent(ActivityUser.this,ActivityRegisterUser.class);
                intent.putExtra("user_id",tvId.getText().toString());

                startActivity(intent);

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFabCreateNewUser:
                startActivity(new Intent(ActivityUser.this, ActivityRegisterUser.class));
                break;
            case R.id.btnFabShowDeactiveUser:
                tvTitleToolbar.setText("مشتریهای غیر فعال");
                adapterUser = new AdapterUser(this, datasource.getAllUsers(0), 0);
                list.setAdapter(adapterUser);
                break;
            case R.id.btnFabShowActiveUser:
                tvTitleToolbar.setText("مشتری های فعال");
                adapterUser = new AdapterUser(this, datasource.getAllUsers(1), 1);
                list.setAdapter(adapterUser);
                break;
        }
    }


    public void DeactiveUserRequest(final Context context, final String user_id) {
        String URL = Misc.Server_Url() + "deactivate_user.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();
                        datasource.deactiveUser(user_id);
                        adapterUser = new AdapterUser(ActivityUser.this, datasource.getAllUsers(1), 1);

                        list.setAdapter(adapterUser);
                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        Misc.setDialog(context, "خطا", "اطلاعات اشتباه", 'e');
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }

    public void ActiveUserRequest(final Context context, final String user_id) {
        String URL = Misc.Server_Url() + "activate_user.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();
                        datasource.activeUser(user_id);
//                        ((BaseAdapter) list.getAdapter()).notifyDataSetChanged();
                        adapterUser = new AdapterUser(ActivityUser.this, datasource.getAllUsers(0), 0);

                        list.setAdapter(adapterUser);

                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        Misc.setDialog(context, "خطا", "اطلاعات اشتباه", 'e');
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }

    @Override
    public void Occured(int user_id, int view) {
        switch (view) {
            case R.id.imgRowUserDelete:
                Log.i("xxx id d", String.valueOf(user_id));
                DeactiveUserRequest(ActivityUser.this, String.valueOf(user_id));
                break;
            case R.id.imgRowUserReturn:
                Log.i("xxx id r", String.valueOf(user_id));
                ActiveUserRequest(ActivityUser.this, String.valueOf(user_id));
                break;
        }
    }
}
