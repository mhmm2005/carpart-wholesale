package com.mansouri.wholesalecarpart.adaptor;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.R;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Car;

import java.util.List;

public class AdapterCarSpinner extends BaseAdapter {

    private static LayoutInflater mInflater = null;

    private Activity activity;
    private List<Car> data;

    public AdapterCarSpinner(Activity a, List<Car> d) {
        activity = a;
        data = d;

        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size()-1;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            row = mInflater.inflate(R.layout.row_spinner_car, parent, false);
            holder = new ViewHolder(row);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        if (data != null && !data.isEmpty()) {
            Car current = data.get(position);
            initValues(holder, current);
        }
        holder.pos = position;

        return row;
    }


    public void initValues(final ViewHolder holder, final Car car) {

        holder.name.setText(car.getName());

        Misc.setFont(activity, holder.name,14,true);

    }

    class ViewHolder {
        TextView  name;
        int pos;

        public ViewHolder(View vi) {
            name = (TextView) vi.findViewById(R.id.tvRowSpinnerCarName);

        }
    }

}