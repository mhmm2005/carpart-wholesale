package com.mansouri.wholesalecarpart.model;

/**
 * Created by Hossein on 2/22/2016.
 */
public class Car {
    int id;
    String name;
    int tag;


    public Car() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }
}
