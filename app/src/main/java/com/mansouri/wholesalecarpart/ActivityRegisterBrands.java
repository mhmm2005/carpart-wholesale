package com.mansouri.wholesalecarpart;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Brand;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ActivityRegisterBrands extends AppCompatActivity implements View.OnClickListener {

    EditText etBrandName;
    Button btnRegisterBrand;
    DATASource datasource;
    Brand brand = new Brand();
    String brand_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_brand);

        initialize();
    }

    private void initialize() {
        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("ثبت مارک");
        Misc.setFont(this, tvTitleToolbar);

        etBrandName = (EditText) findViewById(R.id.etBrandName);
        btnRegisterBrand = (Button) findViewById(R.id.btnRegisterBrand);

        Misc.setFont(this, etBrandName,25,false);
        Misc.setFont(this, btnRegisterBrand,18,true);

        btnRegisterBrand.setOnClickListener(this);
        try {
            brand_id = getIntent().getExtras().getString("brand_id");
            brand = datasource.findBrandById(Integer.parseInt(brand_id));
            etBrandName.setText(brand.getName());

        } catch (Exception e) {

        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegisterBrand:
                if (etBrandName.getText().toString().trim().equals("")) {
                    RelativeLayout rel = (RelativeLayout) findViewById(R.id.relb);
                    Misc.snack(ActivityRegisterBrands.this, rel, "لطفا تمامی فیلد ها را پر نمایید");
                } else {

                    if (brand_id.equals("")) {
                        brand.setId(-1);
                    }

                    brand.setName(etBrandName.getText().toString());

                    registerBrandRequest(ActivityRegisterBrands.this, brand);
                }
                break;
        }
    }


    public void registerBrandRequest(final Context context,final Brand brand) {
        String URL = Misc.Server_Url() + "register_brand.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();

                        String user_id = obj.getString("brand_id");
                        brand.setId(Integer.parseInt(user_id));
                        datasource.createOrUpdateBrand(brand);

                        new SweetAlertDialog(ActivityRegisterBrands.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("پیام سیستم")
                                .setContentText("اطلاعات با موفقیت ثبت گردید")
                                .setConfirmText("خب")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        finish();
                                    }
                                })
                                .show();

                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        Misc.setDialog(context, "خطا", "اطلاعات اشتباه", 'e');
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(brand.getId()));
                params.put("name", brand.getName());
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }

}
