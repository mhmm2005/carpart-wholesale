package com.mansouri.wholesalecarpart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.adaptor.AdapterBrand;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;

public class ActivitySelectBrand extends AppCompatActivity {

    ListView list;
    DATASource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_brand);

        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        list.setAdapter(new AdapterBrand(this, datasource.getAllBrands()));
    }

    private void initialize() {

        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("انتخاب مارک");
        Misc.setFont(this, tvTitleToolbar);

        list = (ListView) findViewById(R.id.listViewSelectBrand);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tvBrand = (TextView) view.findViewById(R.id.tvRowBrand);
                Intent intent = new Intent();
                Log.i("xxx brand", tvBrand.getText().toString());
                intent.putExtra("brand", tvBrand.getText().toString());

                setResult(1000, intent);
                finish();
            }
        });

    }

}
