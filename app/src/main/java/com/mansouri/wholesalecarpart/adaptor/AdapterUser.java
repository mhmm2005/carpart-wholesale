package com.mansouri.wholesalecarpart.adaptor;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.R;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.User;

import java.util.List;


public class AdapterUser extends BaseAdapter {

    private static LayoutInflater inflater = null;
    DATASource datasource;
    int activeUser;
    ComponentClickListener componentClickListener;
    private Activity activity;
    private List<User> data;

    public AdapterUser(){

    }

    public AdapterUser(Activity a, List<User> d, int active) {
        activity = a;
        data = d;
        datasource = new DATASource(activity);
        activeUser = active;
        componentClickListener = (ComponentClickListener) a;
//        datasource.open();

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.row_user, null);
        }

        TextView tvUserId = (TextView) vi.findViewById(R.id.tvRowUserId);
        TextView tvUserCompany = (TextView) vi.findViewById(R.id.tvRowUserCompany);
        TextView tvUserFullName = (TextView) vi.findViewById(R.id.tvRowUserFullName);
        TextView tvUserMobile = (TextView) vi.findViewById(R.id.tvRowUserMobile);

        ImageView imgDelete = (ImageView) vi.findViewById(R.id.imgRowUserDelete);
        ImageView imgReturn = (ImageView) vi.findViewById(R.id.imgRowUserReturn);

        if (activeUser == 1) {
            imgDelete.setVisibility(View.VISIBLE);
            imgReturn.setVisibility(View.INVISIBLE);
        } else {
            imgReturn.setVisibility(View.VISIBLE);
            imgDelete.setVisibility(View.INVISIBLE);
        }

        Misc.setFont(activity, tvUserCompany, 18, true);
        Misc.setFont(activity, tvUserFullName, 14, true);
        Misc.setFont(activity, tvUserMobile, 14, true);

        final User user = data.get(position);

        tvUserId.setText(String.valueOf(user.getId()));
        tvUserFullName.setText("نام: "+user.getFullName());
        tvUserCompany.setText("شرکت: "+user.getCompany());
        tvUserMobile.setText("موبایل: "+user.getMobile());


        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                componentClickListener.Occured(user.getId(),v.getId());
            }
        });

        imgReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                componentClickListener.Occured(user.getId(),v.getId());
            }
        });

        return vi;
    }

    public void setOnComponentItemClickListener(ComponentClickListener componentClickListener) {
        this.componentClickListener = componentClickListener;
    }

    public interface ComponentClickListener {
        void Occured(int user_id, int view);
    }


}