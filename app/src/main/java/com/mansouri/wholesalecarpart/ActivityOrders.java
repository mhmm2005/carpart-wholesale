package com.mansouri.wholesalecarpart;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.adaptor.AdapterOrder;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Order;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivityOrders extends AppCompatActivity implements View.OnClickListener {
    FloatingActionButton btnRefresh;
    ListView list;
    DATASource datasource;
    String status = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);


        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("xxx status onresume", status);
        if (status.equals("0")) {
            getAllOrdersRequest(ActivityOrders.this, String.valueOf(Misc.getOrderLastUpdate(ActivityOrders.this)));
        } else if (status.equals("1")) {
            getAllArchivesRequest(ActivityOrders.this, String.valueOf(Misc.getArchiveLastUpdate(ActivityOrders.this)));
        } else if (status.equals("2")) {
            list.setAdapter(new AdapterOrder(ActivityOrders.this, datasource.getAllInCompleteOrders()));
        }
    }

    private void initialize() {
        status = getIntent().getExtras().getString("value");
        Log.i("xxx status", status);


        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        Misc.setFont(this, tvTitleToolbar);

        if (status.equals("1")) {
            tvTitleToolbar.setText("آرشیو سفارش ها");
        } else if (status.equals("0")) {
            tvTitleToolbar.setText("سفارش های جدید");
        } else if (status.equals("2")) {
            tvTitleToolbar.setText("سفارش های ناتمام");
        }

        btnRefresh = (FloatingActionButton) findViewById(R.id.btnFabRefreshOrder);
        btnRefresh.setOnClickListener(this);
        list = (ListView) findViewById(R.id.listViewOrder);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // namayeshe joziate marboot be order
                TextView tvId = (TextView) view.findViewById(R.id.tvRowOrderId);
                String strId = tvId.getText().toString().substring(13);
                Log.i("xxx orderId", strId);
                Intent intent = new Intent(ActivityOrders.this, ActivityDetailsOrder.class);
                intent.putExtra("order_id", strId);
                intent.putExtra("status", status);


                startActivity(intent);
            }
        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFabRefreshOrder:
                if (status.equals("0")) {
                    getAllOrdersRequest(ActivityOrders.this, String.valueOf(Misc.getOrderLastUpdate(ActivityOrders.this)));
                } else if (status.equals("1")) {
                    getAllArchivesRequest(ActivityOrders.this, String.valueOf(Misc.getArchiveLastUpdate(ActivityOrders.this)));
                }else if (status.equals("2")) {
                    list.setAdapter(new AdapterOrder(ActivityOrders.this, datasource.getAllInCompleteOrders()));
                }
                break;
        }
    }

    public void getAllOrdersRequest(final Context context, final String lastUpdate) {
        String URL = Misc.Server_Url() + "get_all_orders.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx getAllOrdersRequest", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    JSONArray jsonArray = obj.getJSONArray("orders");

                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();

                        // amaliate zakhire orders dar DB ba maghadire ersal shode az json samte server
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonRow = jsonArray.getJSONObject(i);

                            Order order = new Order();
                            order.setId(jsonRow.getInt("id"));
                            order.setUser_id(jsonRow.getInt("user_id"));
                            order.setOrder_time(jsonRow.getLong("order_time"));
                            order.setDescription(jsonRow.getString("description"));
                            order.setAccepted(jsonRow.getInt("accepted"));
                            order.setMessage(jsonRow.getString("message"));
                            order.setOrder(jsonRow.getString("orders"));

                            if (datasource.findUserById(String.valueOf(order.getUser_id())).getActive() == 1) {
                                datasource.createOrUpdateOrder(order);
                            }

                        }


                        // namayesh dar listview
                        list.setAdapter(new AdapterOrder(ActivityOrders.this, datasource.getAllOrders()));

                        // set shodane meghdare lastupdate dar preference
                        long lastUpdate = obj.getLong("last_update");
                        Misc.setOrderLastUpdate(ActivityOrders.this, lastUpdate);


                    }

                    if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        // namayesh dar listview
                        list.setAdapter(new AdapterOrder(ActivityOrders.this, datasource.getAllOrders()));

                        long lastUpdate = obj.getLong("last_update");
                        Misc.setOrderLastUpdate(ActivityOrders.this, lastUpdate);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("last_update", lastUpdate);
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }


    public void getAllArchivesRequest(final Context context, final String lastUpdate) {
        String URL = Misc.Server_Url() + "get_all_archives.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    JSONArray jsonArray = obj.getJSONArray("orders");

                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();

                        // amaliate zakhire orders dar DB ba maghadire ersal shode az json samte server
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonRow = jsonArray.getJSONObject(i);

                            Order order = new Order();
                            order.setId(jsonRow.getInt("id"));
                            order.setUser_id(jsonRow.getInt("user_id"));
                            order.setOrder_time(jsonRow.getLong("order_time"));
                            order.setDescription(jsonRow.getString("description"));
                            order.setAccepted(jsonRow.getInt("accepted"));
                            order.setOrder(jsonRow.getString("orders"));

                            datasource.createOrUpdateOrder(order);

                        }


                        // namayesh dar listview
                        list.setAdapter(new AdapterOrder(ActivityOrders.this, datasource.getAllArchives()));

                        // set shodane meghdare lastupdate dar preference
                        long lastUpdate = obj.getLong("last_update");
                        Misc.setArchiveLastUpdate(ActivityOrders.this, lastUpdate);


                    }

                    if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        // namayesh dar listview
                        list.setAdapter(new AdapterOrder(ActivityOrders.this, datasource.getAllArchives()));

                        long lastUpdate = obj.getLong("last_update");
                        Misc.setArchiveLastUpdate(ActivityOrders.this, lastUpdate);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("last_update", lastUpdate);
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }

}
