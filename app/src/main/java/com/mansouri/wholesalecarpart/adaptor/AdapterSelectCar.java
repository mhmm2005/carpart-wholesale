package com.mansouri.wholesalecarpart.adaptor;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.R;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Car;

import java.util.ArrayList;
import java.util.List;


public class AdapterSelectCar extends BaseAdapter {

    private static LayoutInflater inflater = null;
    DATASource datasource;
    private Activity activity;
    private List<Car> data;

    public AdapterSelectCar(Activity a, List<Car> d, String[] cars) {
        activity = a;
        data = d;
        datasource = new DATASource(activity);
//        datasource.open();

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < cars.length; i++) {
            for (int k = 0; k < data.size(); k++) {
                if(data.get(k).getName().equals(cars[i])){
                    data.get(k).setTag(1);
                }
            }
        }

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public String getSelectedCarNames(){
        String cars="";
        for (int i = 0; i < data.size(); i++) {
            if(data.get(i).getTag()==1){
                cars += data.get(i).getName() + ",";
            }
        }
        if(cars.length()>0){
            cars = cars.substring(0,cars.length()-1);
        }
        return cars;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.row_select_car, null);
        }

        TextView tvCarId = (TextView) vi.findViewById(R.id.tvRowSelectCarId);
        final CheckBox chkCarName = (CheckBox) vi.findViewById(R.id.chkSelectCar);

        Misc.setFont(activity, chkCarName, 16, true);

        Car car = data.get(position);

        tvCarId.setText(String.valueOf(car.getId()));
        chkCarName.setText(car.getName());

        if (data.get(position).getTag() == 1) {
            chkCarName.setChecked(true);
        } else {
            chkCarName.setChecked(false);
        }

        chkCarName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkCarName.isChecked()) {
                    chkCarName.setChecked(true);
                    data.get(position).setTag(1);
                    Log.i("xxx position-chk",position + " " + chkCarName.getText().toString());
                } else {
                    chkCarName.setChecked(false);
                    data.get(position).setTag(0);
                    Log.i("xxx e position-chk", position + " " + chkCarName.getText().toString());
                }
            }
        });

        return vi;
    }

}