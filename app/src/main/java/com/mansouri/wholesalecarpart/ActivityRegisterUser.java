package com.mansouri.wholesalecarpart;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ActivityRegisterUser extends AppCompatActivity implements View.OnClickListener {

    Button btnRegisterUser;
    EditText etFullName, etCompanyName, etMelliCard, etLandLine, etMobile, etAddress, etPassword;
    LinearLayout linRegisterUser;
    DATASource datasource;
    User user = new User();
    String user_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);


        initialize();
    }

    private void initialize() {
        datasource = new DATASource(this);
        btnRegisterUser = (Button) findViewById(R.id.btnRegisterUser);
        etFullName = (EditText) findViewById(R.id.etFullName);
        etCompanyName = (EditText) findViewById(R.id.etCompanyName);
        etMelliCard = (EditText) findViewById(R.id.etMelliCard);
        etLandLine = (EditText) findViewById(R.id.etLandLine);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etAddress = (EditText) findViewById(R.id.etAddress);
        etPassword = (EditText) findViewById(R.id.etUserPassword);
        linRegisterUser = (LinearLayout) findViewById(R.id.linRegisterUser);
        btnRegisterUser.setOnClickListener(this);


        try {
            user_id = getIntent().getExtras().getString("user_id");
            user = datasource.findUserById(user_id);

            etFullName.setText(user.getFullName());
            etCompanyName.setText(user.getCompany());
            etMelliCard.setText(user.getMelliCard());
            etLandLine.setText(user.getLandLine());
            etMobile.setText(user.getMobile());
            etAddress.setText(user.getAddress());
            etPassword.setText(user.getPassword());

        } catch (Exception e) {

        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("ثبت مشتری");
        Misc.setFont(this, tvTitleToolbar);


        Misc.setFont(this, btnRegisterUser,18,true);
        Misc.setFont(this, etFullName,25,false);
        Misc.setFont(this, etCompanyName,25,false);
        Misc.setFont(this, etMelliCard,25,false);
        Misc.setFont(this, etLandLine,25,false);
        Misc.setFont(this, etMobile,25,false);
        Misc.setFont(this, etAddress,25,false);
        Misc.setFont(this, etPassword,25,false);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegisterUser:
                if (isUserEditTextEmpty()) {
                    RelativeLayout rel = (RelativeLayout) findViewById(R.id.reluser);
                    Misc.snack(ActivityRegisterUser.this, rel, "لطفا تمامی فیلد ها را پر نمایید");
                } else {

                    if (user_id.equals("")) {
                        user.setId(-1);
                        user.setActive(1);
                    }

                    user.setFullName(etFullName.getText().toString());
                    user.setMobile(etMobile.getText().toString());
                    user.setLandLine(etLandLine.getText().toString());
                    user.setMelliCard(etMelliCard.getText().toString());
                    user.setCompany(etCompanyName.getText().toString());
                    user.setAddress(etAddress.getText().toString());
                    user.setPassword(etPassword.getText().toString());

                    registerUserRequest(ActivityRegisterUser.this, user);
                }
                break;
        }
    }

    private boolean isUserEditTextEmpty() {

        if (
                etFullName.getText().toString().trim() == "" ||
                        etMobile.getText().toString().trim().equals("") ||
                        etLandLine.getText().toString().trim().equals("") ||
                        etMelliCard.getText().toString().trim().equals("") ||
                        etCompanyName.getText().toString().trim().equals("") ||
                        etAddress.getText().toString().trim().equals("") ||
                        etPassword.getText().toString().trim().equals("")
                ) {
            return true;

        } else {
            return false;
        }
    }

    public void registerUserRequest(final Context context, final User user) {
        String URL = Misc.Server_Url() + "register_user.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();

                        String user_id = obj.getString("user_id");
                        user.setId(Integer.parseInt(user_id));
                        datasource.saveOrUpdateUser(user);

                        new SweetAlertDialog(ActivityRegisterUser.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("پیام سیستم")
                                .setContentText("اطلاعات با موفقیت ثبت گردید")
                                .setConfirmText("خب")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        finish();
                                    }
                                })
                                .show();

                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        Misc.setDialog(context, "خطا", "دوباره تلاش کنید", 'e');
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(user.getId()));
                params.put("fullname", user.getFullName());
                params.put("company", user.getCompany());
                params.put("mellicard", user.getMelliCard());
                params.put("landline", user.getLandLine());
                params.put("mobile", user.getMobile());
                params.put("address", user.getAddress());
                params.put("password", user.getPassword());
                params.put("active", String.valueOf(user.getActive()));
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }

}
