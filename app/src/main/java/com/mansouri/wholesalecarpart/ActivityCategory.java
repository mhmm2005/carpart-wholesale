package com.mansouri.wholesalecarpart;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.adaptor.AdapterCar;
import com.mansouri.wholesalecarpart.adaptor.AdapterCategory;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;

public class ActivityCategory extends AppCompatActivity implements View.OnClickListener {

    FloatingActionButton btnNewCategory;
    ListView list;
    DATASource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);


        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        list.setAdapter(new AdapterCategory(this, datasource.getAllCategories()));

    }

    private void initialize() {
        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("گروه بندی");
        Misc.setFont(this, tvTitleToolbar);

        btnNewCategory = (FloatingActionButton) findViewById(R.id.btnFabCreateNewCategory);
        btnNewCategory.setOnClickListener(this);

        list = (ListView) findViewById(R.id.listViewCategory);
//        list.setAdapter(new AdapterCategory(this, datasource.getAllCategories()));

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tvId = (TextView) view.findViewById(R.id.tvRowCategoryId);
                Intent intent = new Intent(ActivityCategory.this, ActivityProduct.class);
                intent.putExtra("category_id", tvId.getText().toString());
                startActivity(intent);
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFabCreateNewCategory:
                Intent intent = new Intent(ActivityCategory.this, ActivityRegisterCategory.class);
                intent.putExtra("category_id","-1");
                startActivity(intent);

                break;
        }
    }
}
