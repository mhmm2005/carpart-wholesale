package com.mansouri.wholesalecarpart;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.adaptor.AdapterCarSpinner;
import com.mansouri.wholesalecarpart.adaptor.AdapterCompaniesSpinner;
import com.mansouri.wholesalecarpart.adaptor.AdapterProduct;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ActivityProduct extends AppCompatActivity implements View.OnClickListener, AdapterProduct.ComponentClickListener {

    String strBrandName, strCarName = "";

    FloatingActionButton btnLSearchProduct, btnCreateNewProduct;
    ListView list;
    DATASource datasource;
    String category_id;
    AdapterProduct adapterProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);


        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapterProduct = new AdapterProduct(this, datasource.getAllProducts(category_id), Integer.valueOf(category_id));
        list.setAdapter(adapterProduct);
    }


    private void initialize() {
        adapterProduct = new AdapterProduct();
        adapterProduct.setOnComponentItemClickListener(this);

        category_id = getIntent().getExtras().getString("category_id");
        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("کالا");
        Misc.setFont(this, tvTitleToolbar);

        btnCreateNewProduct = (FloatingActionButton) findViewById(R.id.btnFabCreateNewProduct);
        btnLSearchProduct = (FloatingActionButton) findViewById(R.id.btnFabLSearchProduct);

        btnCreateNewProduct.setOnClickListener(this);
        btnLSearchProduct.setOnClickListener(this);

        list = (ListView) findViewById(R.id.listViewProduct);
//        list.setAdapter(new AdapterProduct(this, datasource.getAllProducts(category_id)));

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tvId = (TextView) view.findViewById(R.id.tvRowProductId);
                Intent intent = new Intent(ActivityProduct.this, ActivityRegisterProduct.class);
                intent.putExtra("product_id", tvId.getText().toString());
                intent.putExtra("category_id", category_id);

                startActivity(intent);
            }
        });
    }

    public void DeleteProductRequest(final Context context, final String Product_id) {
        String URL = Misc.Server_Url() + "delete_product.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Misc.dismissDialogProgress();

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        datasource.deleteProduct(Product_id);
                        adapterProduct = new AdapterProduct(ActivityProduct.this, datasource.getAllProducts(category_id), Integer.valueOf(category_id));
                        list.setAdapter(adapterProduct);

                        String filePath = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_PICTURES + "/carpart/product/p" + Product_id + ".png";
                        File file = new File(filePath);
                        if (file.exists()) file.delete();

                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.setDialog(context, "خطا", "دوباره تلاش کنید", 'e');
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');

//                datasource.deleteProduct(Product_id);
//                adapterProduct = new AdapterProduct(ActivityProduct.this, datasource.getAllProducts(category_id), Integer.valueOf(category_id));
//                list.setAdapter(adapterProduct);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("product_id", Product_id);
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFabCreateNewProduct:
                Intent intent = new Intent(ActivityProduct.this, ActivityRegisterProduct.class);
                intent.putExtra("category_id", category_id);
                intent.putExtra("product_id", "-1");

                startActivity(intent);

                break;
            case R.id.btnFabLSearchProduct:
                final int category_id = Integer.parseInt(getIntent().getExtras().getString("category_id"));
                final Dialog d = new Dialog(ActivityProduct.this);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.activity_search);


                final EditText etSearchName = (EditText) d.findViewById(R.id.etSearchName);
                final Spinner spinnerBrand = (Spinner) d.findViewById(R.id.spinnerBrand);
                final Spinner spinnerCar = (Spinner) d.findViewById(R.id.spinnerCar);

                Button btnSearch = (Button) d.findViewById(R.id.btnProductsSearch);

                Misc.setFont(ActivityProduct.this, etSearchName);
                Misc.setFont(ActivityProduct.this, btnSearch);


                spinnerBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        TextView tvCompanyId = (TextView) view.findViewById(R.id.tvRowSpinnerCompanyId);
                        strBrandName = tvCompanyId.getText().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spinnerCar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        TextView tvCar = (TextView) view.findViewById(R.id.tvRowSpinnerCarName);
                        strCarName = tvCar.getText().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spinnerBrand.setAdapter(new AdapterCompaniesSpinner(ActivityProduct.this, datasource.AllCompanies()));
                spinnerBrand.setSelection(datasource.AllCompanies().size() - 1);

                spinnerCar.setAdapter(new AdapterCarSpinner(ActivityProduct.this, datasource.AllCars()));
                spinnerCar.setSelection(datasource.AllCars().size() - 1);

                btnSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.dismiss();

                        list.setAdapter(new AdapterProduct(ActivityProduct.this,
                                datasource.searchAdvanceProducts(
                                        etSearchName.getText().toString(),
                                        strBrandName,
                                        strCarName,
                                        category_id),
                                Integer.parseInt(getIntent().getExtras().getString("category_id"))));
                    }
                });
                d.show();
                break;
        }
    }


    @Override
    public void Occured(int product_id) {
        DeleteProductRequest(ActivityProduct.this, String.valueOf(product_id));
    }
}
