package com.mansouri.wholesalecarpart.adaptor;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.R;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Brand;

import java.util.List;


public class AdapterBrand extends BaseAdapter {

    private static LayoutInflater inflater = null;
    DATASource datasource;
    private Activity activity;
    private List<Brand> data;

    public AdapterBrand(Activity a, List<Brand> d) {
        activity = a;
        data = d;
        datasource = new DATASource(activity);
//        datasource.open();

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.row_brand, null);
        }

        TextView tvBrandId = (TextView) vi.findViewById(R.id.tvRowBrandId);
        TextView tvBrandName = (TextView) vi.findViewById(R.id.tvRowBrand);

        Misc.setFont(activity, tvBrandName, 16, true);

        Brand brand = data.get(position);

        tvBrandId.setText(String.valueOf(brand.getId()));
        tvBrandName.setText(brand.getName());

        return vi;
    }

}