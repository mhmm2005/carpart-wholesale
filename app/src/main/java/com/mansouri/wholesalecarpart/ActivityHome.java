package com.mansouri.wholesalecarpart;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Brand;
import com.mansouri.wholesalecarpart.model.Car;
import com.mansouri.wholesalecarpart.model.Category;
import com.mansouri.wholesalecarpart.model.Order;
import com.mansouri.wholesalecarpart.model.Product;
import com.mansouri.wholesalecarpart.model.User;
import com.mansouri.wholesalecarpart.services.checkForNewOrder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivityHome extends AppCompatActivity implements View.OnClickListener {

    Button btnFetchAllData, btnCustomer, btnBrand, btnVehicle, btnCategory, btnNewShowOrders, btnShowIncompleteOrders, btnShowArchiveOrders;
    DATASource datasource;
    int flag1 = 0, flag2 = 0, flag3 = 0, flag4 = 0, flag5 = 0, flag6 = 0;
    RelativeLayout rel;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initialize();
    }

    private void initialize() {

        rel = (RelativeLayout) findViewById(R.id.rel);
        datasource = new DATASource(this);

        Intent intent = new Intent(this, checkForNewOrder.class);
        startService(intent);

        try {
            NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            nMgr.cancel(0);
        } catch (Exception e) {

        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("کارپارت");
        Misc.setFont(this, tvTitleToolbar);

        btnCustomer = (Button) findViewById(R.id.btnCustomer);
        btnBrand = (Button) findViewById(R.id.btnBrand);
        btnVehicle = (Button) findViewById(R.id.btnVehicle);
        btnCategory = (Button) findViewById(R.id.btnCategory);
        btnNewShowOrders = (Button) findViewById(R.id.btnShowNewOrders);
        btnShowIncompleteOrders = (Button) findViewById(R.id.btnShowIncompleteOrders);
        btnShowArchiveOrders = (Button) findViewById(R.id.btnShowArchiveOrders);
        btnFetchAllData = (Button) findViewById(R.id.btnFetchAllData);

        btnCustomer.setOnClickListener(this);
        btnBrand.setOnClickListener(this);
        btnVehicle.setOnClickListener(this);
        btnCategory.setOnClickListener(this);
        btnNewShowOrders.setOnClickListener(this);
        btnShowIncompleteOrders.setOnClickListener(this);
        btnShowArchiveOrders.setOnClickListener(this);
        btnFetchAllData.setOnClickListener(this);

        Misc.setFont(this, btnCustomer,16,true);
        Misc.setFont(this, btnBrand,16,true);
        Misc.setFont(this, btnVehicle,16,true);
        Misc.setFont(this, btnCategory,16,true);
        Misc.setFont(this, btnNewShowOrders,16,true);
        Misc.setFont(this, btnShowIncompleteOrders,16,true);
        Misc.setFont(this, btnShowArchiveOrders,16,true);
        Misc.setFont(this, btnFetchAllData,16,true);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()) {
            case R.id.btnCustomer:
                intent.setClass(ActivityHome.this, ActivityUser.class);
                startActivity(intent);
                break;
            case R.id.btnBrand:
                intent.setClass(ActivityHome.this, ActivityBrand.class);
                startActivity(intent);
                break;
            case R.id.btnVehicle:
                intent.setClass(ActivityHome.this, ActivityCar.class);
                startActivity(intent);
                break;
            case R.id.btnCategory:
                intent.setClass(ActivityHome.this, ActivityCategory.class);
                startActivity(intent);
                break;
            case R.id.btnShowNewOrders:
                try {
                    NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    nMgr.cancel(0);
                } catch (Exception e) {

                }
                intent.setClass(ActivityHome.this, ActivityOrders.class);
                intent.putExtra("value", "0");
                startActivity(intent);
                break;
            case R.id.btnShowArchiveOrders:
                intent.setClass(ActivityHome.this, ActivityOrders.class);
                intent.putExtra("value", "1");
                startActivity(intent);
                break;
            case R.id.btnShowIncompleteOrders:
                intent.setClass(ActivityHome.this, ActivityOrders.class);
                intent.putExtra("value", "2");
                startActivity(intent);
                break;
            case R.id.btnFetchAllData:

                getAllCategories(1000);
                getAllCompanies(1000);
                getAllCars(1000);
                getAllUsers();
                getAllProducts(1000);
                getAllOrders();

                break;
        }
    }

    public void getAllCategories(final long LastUpdate) {
        datasource.deleteAllCategories();
        Log.i("xxx getAllCategories","getAllCategories");

        String URL = Misc.Server_Url() + "get_all_categories.php";
        flag1 = 0;

        StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                flag1 = 1;
                if (flag1 == 1 && flag2 == 1 && flag3 == 1 && flag4 == 1 && flag5 == 1 && flag6 == 1) {
                    btnFetchAllData.setEnabled(true);
                } else {
                    btnFetchAllData.setEnabled(false);
                }

                try {
                    JSONObject res = new JSONObject(response);
                    Misc.setLastUpdateCategory(ActivityHome.this, res.getLong("last_update"));

                    if (res.getInt("success") == 1) {
                        JSONArray jsonArrayCategories = res.getJSONArray("categories");
                        Log.i("xxx", "succeeded");
                        try {
                            for (int i = 0; i < jsonArrayCategories.length(); i++) {
                                JSONObject obj = new JSONObject();
                                obj = jsonArrayCategories.getJSONObject(i);
                                Category category = new Category();
                                category.setId(obj.getInt("id"));
                                category.setName(obj.getString("name"));

                                datasource.createOrUpdateCategory(category);

                            }

                            Misc.freeMemory();
                            Misc.snack(ActivityHome.this, rel, "لیست گروه بندی بروز شد");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            flag1 = 1;
                        }
                    }
                    if (res.getInt("success") == 0) {
                        flag1 = 1;

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    flag1 = 1;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Misc.setDialog(ActivityHome.this, "خطا", "اشکال در ارتباط با سرور", 'e');
                flag1 = 1;

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("last_update", String.valueOf(LastUpdate));
                return params;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(10 * 1000, 5, 2);
        sr.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(sr);
    }

    public void getAllCompanies(final long LastUpdate) {
        Log.i("xxx getAllCompanies","getAllCompanies");
        datasource.deleteAllCompanies();
        String URL = Misc.Server_Url() + "get_companies.php";
        flag2 = 0;

        StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                flag2 = 1;
                if (flag1 == 1 && flag2 == 1 && flag3 == 1 && flag4 == 1 && flag5 == 1 && flag6 == 1) {
                    btnFetchAllData.setEnabled(true);
                } else {
                    btnFetchAllData.setEnabled(false);
                }
                Log.i("xxx", response.toString());

                try {
                    JSONObject res = new JSONObject(response);

                    if (res.getInt("success") == 1) {
                        JSONArray jsonArray = res.getJSONArray("companies");
                        Log.i("xxx", "succeeded");
                        try {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Brand brand = new Brand();
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                brand.setId(jsonObject.getInt("id"));
                                brand.setName(jsonObject.getString("name"));
                                datasource.createOrUpdateCompany(brand);
                            }
                            Misc.freeMemory();
                            Misc.snack(ActivityHome.this, rel, "لیست شرکت ها بروز شد");

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Misc.dismissDialogProgress();
                        }
                    }
                    if (res.getInt("success") == 0) {
//                        Misc.SetDialog(ActivityMain.this, "خطا", res.getString("message"), 'w');
                        flag2 = 1;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    flag2 = 1;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Misc.SetDialog(ActivityMain.this, "خطا", "اشکال در ارتباط با سرور", 'e');
                flag2 = 1;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("last_update", String.valueOf(LastUpdate));
                return params;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(10 * 1000, 5, 2);
        sr.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(sr);
    }

    public void getAllCars(final long LastUpdate) {
        Log.i("xxx getAllCars","getAllCars");
        datasource.deleteAllCars();
        String URL = Misc.Server_Url() + "get_cars.php";
        flag3 = 0;

        StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                flag3 = 1;
                if (flag1 == 1 && flag2 == 1 && flag3 == 1 && flag4 == 1 && flag5 == 1 && flag6 == 1) {
                    btnFetchAllData.setEnabled(true);
                } else {
                    btnFetchAllData.setEnabled(false);
                }

                try {
                    JSONObject res = new JSONObject(response);

                    if (res.getInt("success") == 1) {
                        JSONArray jsonArray = res.getJSONArray("cars");
                        Log.i("xxx", "succeeded");
                        try {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Car car = new Car();
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                car.setId(jsonObject.getInt("id"));
                                car.setName(jsonObject.getString("name"));
                                datasource.createOrUpdateCar(car);
                            }
                            Misc.freeMemory();
                            Misc.snack(ActivityHome.this, rel, "لیست خودرو ها بروز شد");


                        } catch (JSONException e) {
                            flag3 = 1;
                            e.printStackTrace();
                        }
                    }
                    if (res.getInt("success") == 0) {
                    }
                } catch (JSONException e) {
                    flag3 = 1;
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                flag3 = 1;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("last_update", String.valueOf(LastUpdate));
                return params;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(10 * 1000, 5, 2);
        sr.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(sr);
    }

    public void getAllUsers() {
        Log.i("xxx deleteAllUsers", "deleteAllUsers");
        datasource.deleteAllUsers();
        String URL = Misc.Server_Url() + "get_all_user.php";
        flag4 = 0;

        StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                flag4 = 1;
                if (flag1 == 1 && flag2 == 1 && flag3 == 1 && flag4 == 1 && flag5 == 1 && flag6 == 1) {
                    btnFetchAllData.setEnabled(true);
                } else {
                    btnFetchAllData.setEnabled(false);
                }
                try {
                    JSONObject res = new JSONObject(response);

                    if (res.getInt("success") == 1) {
                        JSONArray jsonArray = res.getJSONArray("users");
                        Log.i("xxx", "succeeded");
                        try {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                User user = new User();
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                user.setId(jsonObject.getInt("id"));
                                user.setFullName(jsonObject.getString("fullname"));
                                user.setCompany(jsonObject.getString("company"));
                                user.setMobile(jsonObject.getString("mobile"));
                                user.setLandLine(jsonObject.getString("landline"));
                                user.setMelliCard(jsonObject.getString("mellicard"));
                                user.setPassword(jsonObject.getString("password"));
                                user.setActive(jsonObject.getInt("active"));
                                user.setAddress(jsonObject.getString("address"));

                                datasource.createOrUpdateUser(user);
                            }
                            Misc.freeMemory();
                            Misc.snack(ActivityHome.this, rel, "لیست مشتری ها بروز شد");


                        } catch (JSONException e) {
                            flag4 = 1;
                            e.printStackTrace();
                        }
                    }
                    if (res.getInt("success") == 0) {
                    }
                } catch (JSONException e) {
                    flag4 = 1;
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                flag4 = 1;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(10 * 1000, 5, 2);
        sr.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(sr);
    }

    public void getAllProducts(final long LastUpdate) {
        Log.i("xxx getAllProducts", "getAllProducts");
        datasource.deleteAllProducts();
        String URL = Misc.Server_Url() + "get_all_products.php";
        flag5 = 0;

        StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                flag5 = 1;
                if (flag1 == 1 && flag2 == 1 && flag3 == 1 && flag4 == 1 && flag5 == 1 && flag6 == 1) {
                    btnFetchAllData.setEnabled(true);
                } else {
                    btnFetchAllData.setEnabled(false);
                }
                try {
                    JSONObject res = new JSONObject(response);

                    if (res.getInt("success") == 1) {
                        JSONArray jsonArray = res.getJSONArray("products");
                        Log.i("xxx", "succeeded");
                        try {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Product product = new Product();
                                JSONObject jsonObject = jsonArray.getJSONObject(i);


                                product.setId(jsonObject.getInt("id"));
                                product.setCategory_id(jsonObject.getInt("category_id"));
                                product.setName(jsonObject.getString("name"));
                                product.setShare_car(jsonObject.getString("sharecar"));
                                product.setPrice(jsonObject.getInt("price"));
                                product.setDescription(jsonObject.getString("description"));
                                product.setPhoto(jsonObject.getInt("photos"));
                                product.setGaranty(jsonObject.getString("garanty"));
                                product.setBox_size(jsonObject.getString("box_size"));
                                product.setBrand_id(jsonObject.getInt("brand_id"));
                                product.setFavorite(jsonObject.getInt("favorite"));
                                product.setActive(jsonObject.getInt("active"));
                                product.setPriority(jsonObject.getInt("priority"));

                                datasource.createOrUpdateProduct(product);
                            }
                            Misc.freeMemory();
                            Misc.snack(ActivityHome.this, rel, "لیست کالاها بروز شد");


                        } catch (JSONException e) {
                            flag5 = 1;
                            e.printStackTrace();
                        }
                    }
                    if (res.getInt("success") == 0) {
                    }
                } catch (JSONException e) {
                    flag5 = 1;
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                flag5 = 1;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("last_update", String.valueOf(LastUpdate));
                return params;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(10 * 1000, 5, 2);
        sr.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(sr);
    }

    public void getAllOrders() {
        Log.i("xxx getAllOrders", "getAllOrders");
        datasource.deleteAllOrders();
        String URL = Misc.Server_Url() + "fetch_all_orders_first_time.php";
        flag6 = 0;

        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                flag6 = 1;
                if (flag1 == 1 && flag2 == 1 && flag3 == 1 && flag4 == 1 && flag5 == 1 && flag6 == 1) {
                    btnFetchAllData.setEnabled(true);
                } else {
                    btnFetchAllData.setEnabled(false);
                }

                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    JSONArray jsonArray = obj.getJSONArray("orders");

                    if (obj.getInt("success") == 1) {

                        // amaliate zakhire orders dar DB ba maghadire ersal shode az json samte server
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonRow = jsonArray.getJSONObject(i);

                            Order order = new Order();
                            order.setId(jsonRow.getInt("id"));
                            order.setUser_id(jsonRow.getInt("user_id"));
                            order.setOrder_time(jsonRow.getLong("order_time"));
                            order.setDescription(jsonRow.getString("description"));
                            order.setAccepted(jsonRow.getInt("accepted"));
                            order.setMessage(jsonRow.getString("message").trim());
                            order.setOrder(jsonRow.getString("orders"));

                            datasource.createOrUpdateOrder(order);

                        }

                        // set shodane meghdare lastupdate dar preference
//                        long lastUpdate = obj.getLong("last_update");
//                        Misc.setOrderLastUpdate(ActivityHome.this, lastUpdate);
                        Misc.snack(ActivityHome.this, rel, "لیست سفارش ها بروز شد");

                    }

                    if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");

//                        long lastUpdate = obj.getLong("last_update");
//                        Misc.setOrderLastUpdate(ActivityHome.this, lastUpdate);
                    }
                } catch (JSONException e) {
                    flag6 = 1;
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                flag6 = 1;
                Log.i("xxx err", error.toString());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
//                params.put("last_update", String.valueOf(lastUpdate));
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Misc.snack(ActivityHome.this, rel, "دکمه برگشت را دوباره فشار دهید");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onDestroy() {
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.set(
                alarm.RTC_WAKEUP,
                System.currentTimeMillis() + (1000 * 60 * 30),
                PendingIntent.getService(this, 0, new Intent(this, checkForNewOrder.class), 0)
        );
        super.onDestroy();

    }
}
