package com.mansouri.wholesalecarpart;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AndroidMultiPartEntity;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Category;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ActivityRegisterCategory extends AppCompatActivity implements View.OnClickListener {

    final int CAMERA_CAPTURE = 1;
    final int PIC_CROP = 2;
    EditText etCategoryName;
    Button btnRegisterCategory, btnSetCategoryImage;
    ImageView imgCategory;
    DATASource datasource;
    Category category = new Category();
    String category_id = "";
    long totalSize = 0;
    private Uri picUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_category);


        initialize();
    }

    private void initialize() {
        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("ثبت گروه بندی");
        Misc.setFont(this, tvTitleToolbar);

        etCategoryName = (EditText) findViewById(R.id.etCategoryName);
        btnRegisterCategory = (Button) findViewById(R.id.btnRegisterCategory);
        btnSetCategoryImage = (Button) findViewById(R.id.btnSetCategoryImage);
        imgCategory = (ImageView) findViewById(R.id.imgCategory);

        Misc.setFont(this, etCategoryName,25,false);
        Misc.setFont(this, btnRegisterCategory,18,true);
        Misc.setFont(this, btnSetCategoryImage,18,true);

        btnRegisterCategory.setOnClickListener(this);
        btnSetCategoryImage.setOnClickListener(this);

        try {
            category_id = getIntent().getExtras().getString("category_id");

            if (!category_id.equals("-1")) {
                Log.i("xxx cat id", category_id);
                category = datasource.findCategoryById(category_id);
                etCategoryName.setText(category.getName());

                btnSetCategoryImage.setVisibility(View.VISIBLE);

                Log.i("xxx image", Misc.Server_Url() + "image/cat" + category.getPhoto() + ".png");
//                Random rnd = new Random();
                Picasso.with(this)
                        .load(Misc.Server_Url() + "image/cat" + category.getPhoto() + ".png?random=" + Misc.getLastUpdateCategory(this))
                        .noFade()
                        .error(R.drawable.placeholder)
                        .placeholder(R.drawable.placeholder)
                        .fit()
                        .into(imgCategory);
            } else {
                btnSetCategoryImage.setVisibility(View.INVISIBLE);
            }

        } catch (Exception e) {

        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegisterCategory:
                if (etCategoryName.getText().toString().trim().equals("")) {
                    RelativeLayout rel = (RelativeLayout) findViewById(R.id.relcat);
                    Misc.snack(ActivityRegisterCategory.this, rel, "لطفا تمامی فیلد ها را پر نمایید");
                } else {

                    if (category_id.equals("-1")) {
                        category.setId(-1);
                    }

                    category.setName(etCategoryName.getText().toString());

                    registerCategoryRequest(ActivityRegisterCategory.this, category);
                }
                break;

            case R.id.btnSetCategoryImage:
                Crop.pickImage(ActivityRegisterCategory.this);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            //user is returning from capturing an image using the camera
            if (requestCode == CAMERA_CAPTURE) {
                //get the Uri for the captured image
                picUri = data.getData();
                //carry out the crop operation
                performCrop();
            } else if (requestCode == PIC_CROP) {
                //user is returning from cropping the image
                //get the returned data
                Bundle extras = data.getExtras();
                //get the cropped bitmap
                Bitmap thePic = extras.getParcelable("data");
                imgCategory.setImageBitmap(thePic);

                saveImage();
            } else if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
                beginCrop(data.getData());
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }

    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            imgCategory.setImageURI(Crop.getOutput(result));
            saveImage();
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void performCrop() {
        //take care of exceptions
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        //respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            //display an error message
//            String errorMessage = "Whoops - your device doesn't support the crop action!";
//            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
//            toast.show();
        }
    }

    private void saveImage() {
        imgCategory.setDrawingCacheEnabled(true);
        imgCategory.buildDrawingCache();
        Bitmap bm = imgCategory.getDrawingCache();
        Random rnd = new Random();
        Misc.setLastUpdateCategory(ActivityRegisterCategory.this, rnd.nextLong());
        String root = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_PICTURES;
        Log.i("xxx root", root);
        File myDir = new File(root + "/carpart/category/");
        myDir.mkdirs();
        File file = new File(myDir, "cat" + category.getId() + ".png");
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        ActivityRegisterCategory.this.sendBroadcast(mediaScanIntent);

//        progressUpload.setVisibility(View.VISIBLE);
        new UploadFileToServer().execute();
    }

    public void registerCategoryRequest(final Context context, final Category category) {
        String URL = Misc.Server_Url() + "register_category.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();

                        String category_id = obj.getString("category_id");
                        category.setId(Integer.parseInt(category_id));
                        category.setPhoto(Integer.parseInt(category_id));
                        datasource.saveOrUpdateCategory(category);
                        btnSetCategoryImage.setVisibility(View.VISIBLE);
                        btnRegisterCategory.setEnabled(false);

                        Misc.setDialog(context, "پیام سیستم", "اطلاعات با موفقیت ثبت گردید", 's');


//                        finish();
                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        Misc.setDialog(context, "خطا", "اطلاعات اشتباه", 'e');
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(category.getId()));
                params.put("name", category.getName());
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            Misc.showDialogProgress(ActivityRegisterCategory.this);

//            progressUpload.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // updating progress bar value
//            progressUpload.setProgress(progress[0]);

        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Misc.Server_Url() + "upload.php");

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });
                String root = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_PICTURES;
                String filePath = root + "/carpart/category/cat" + category.getId() + ".png";
                Log.i("xxx path", filePath);
                File sourceFile = new File(filePath);

                // Adding file data to http body
                entity.addPart("image", new FileBody(sourceFile));
//                entity.addPart("category_id",new StringBody(String.valueOf(category.getId())));

                // Extra parameters if you want to pass to server
//                entity.addPart("email", new StringBody("abc@gmail.com"));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = "1";


                } else {
//                    Misc.dismissDialogProgress();

                    responseString = "0";
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {


            super.onPostExecute(result);
            Misc.dismissDialogProgress();
            if (result.equals("1")) {
//                category.setPhoto(category.getId());
//                registerCategoryRequest(ActivityRegisterCategory.this, category);
            } else {
                Toast.makeText(ActivityRegisterCategory.this, "Upload failed", Toast.LENGTH_SHORT).show();
            }


//            finish();
        }

    }

}
