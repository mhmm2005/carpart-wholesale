package com.mansouri.wholesalecarpart.library;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PersianDate {
    static int intDay, intMonth, intYear;
    static int intShamsDay, intShamsMonth, intShamsYear;
    static String strShamsDay, strShamsMonth, strShamsYear;

    public static String Shamsi(int day, int month, int year) {
        intDay = day;
        intMonth = month;
        intYear = year;
		
		if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)) && month >2){
			++intDay;
		}

        switch (month) {
            case 1:
                intShamsYear = intYear - 622;
                if (intDay <= 20) {
                    intShamsMonth = 10;
                    intShamsDay = intDay + 10;
                } else {
                    intShamsMonth = 11;
                    intShamsDay = intDay - 20;
                }
                break;
            case 2:
                intShamsYear = intYear - 622;
                if (intDay <= 19) {
                    intShamsMonth = 11;
                    intShamsDay = intDay + 11;
                } else {
                    intShamsMonth = 12;
                    intShamsDay = intDay - 19;
                }
                break;
            case 3:
                if (intDay <= 20) {
                    intShamsMonth = 12;
                    intShamsDay = intDay + 9;
                    intShamsYear = intYear - 622;
                } else {
                    intShamsMonth = 1;
                    intShamsDay = intDay - 20;
                    intShamsYear = intYear - 621;
                }
                break;
            case 4:
                intShamsYear = intYear - 621;
                if (intDay <= 20) {
                    intShamsMonth = 1;
                    intShamsDay = intDay + 11;
                } else {
                    intShamsMonth = 2;
                    intShamsDay = intDay - 20;
                }
                break;
            case 5:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 2;
                    intShamsDay = intDay + 10;
                } else {
                    intShamsMonth = 3;
                    intShamsDay = intDay - 21;
                }
                break;
            case 6:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 3;
                    intShamsDay = intDay + 10;
                } else {
                    intShamsMonth = 4;
                    intShamsDay = intDay - 21;
                }
                break;
            case 7:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 4;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 5;
                    intShamsDay = intDay - 22;
                }
                break;
            case 8:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 5;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 6;
                    intShamsDay = intDay - 22;
                }
                break;
            case 9:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 6;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 7;
                    intShamsDay = intDay - 22;
                }
                break;
            case 10:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 7;
                    intShamsDay = intDay + 8;
                } else {
                    intShamsMonth = 8;
                    intShamsDay = intDay - 22;
                }
                break;
            case 11:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 8;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 9;
                    intShamsDay = intDay - 21;
                }
                break;
            case 12:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 9;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 10;
                    intShamsDay = intDay - 21;
                }
                break;

        }

        if (intShamsDay < 10) {
            strShamsDay = "0" + String.valueOf(intShamsDay);
        } else {
            strShamsDay = String.valueOf(intShamsDay);
        }
        if (intShamsMonth < 10) {
            strShamsMonth = "0" + String.valueOf(intShamsMonth);
        } else {
            strShamsMonth = String.valueOf(intShamsMonth);
        }
        strShamsYear = String.valueOf(intShamsYear);

        return strShamsYear + "/" + strShamsMonth + "/" + strShamsDay;

    }

    public static String Shamsi() {

        int day, month, year;
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        String dt = formatter.format(today);
//        Log.i("xxx cal", dt);
        String[] arrayDate = dt.split("-");

        day = Integer.valueOf(arrayDate[0]);
//        Log.i("xxx day", day + "");

        month = Integer.valueOf(arrayDate[1]);
//        Log.i("xxx month", month + "");

        year = Integer.valueOf(arrayDate[2]);
//        Log.i("xxx year", year + "");





        intDay = day;
        intMonth = month;
        intYear = year;
		
		if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)) && month >2){
			++intDay;
		}

        switch (month) {
            case 1:
                intShamsYear = intYear - 622;
                if (intDay <= 20) {
                    intShamsMonth = 10;
                    intShamsDay = intDay + 10;
                } else {
                    intShamsMonth = 11;
                    intShamsDay = intDay - 20;
                }
                break;
            case 2:
                intShamsYear = intYear - 622;
                if (intDay <= 19) {
                    intShamsMonth = 11;
                    intShamsDay = intDay + 11;
                } else {
                    intShamsMonth = 12;
                    intShamsDay = intDay - 19;
                }
                break;
            case 3:
                if (intDay <= 20) {
                    intShamsMonth = 12;
                    intShamsDay = intDay + 9;
                    intShamsYear = intYear - 622;
                } else {
                    intShamsMonth = 1;
                    intShamsDay = intDay - 20;
                    intShamsYear = intYear - 621;
                }
                break;
            case 4:
                intShamsYear = intYear - 621;
                if (intDay <= 20) {
                    intShamsMonth = 1;
                    intShamsDay = intDay + 11;
                } else {
                    intShamsMonth = 2;
                    intShamsDay = intDay - 20;
                }
                break;
            case 5:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 2;
                    intShamsDay = intDay + 10;
                } else {
                    intShamsMonth = 3;
                    intShamsDay = intDay - 21;
                }
                break;
            case 6:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 3;
                    intShamsDay = intDay + 10;
                } else {
                    intShamsMonth = 4;
                    intShamsDay = intDay - 21;
                }
                break;
            case 7:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 4;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 5;
                    intShamsDay = intDay - 22;
                }
                break;
            case 8:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 5;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 6;
                    intShamsDay = intDay - 22;
                }
                break;
            case 9:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 6;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 7;
                    intShamsDay = intDay - 22;
                }
                break;
            case 10:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 7;
                    intShamsDay = intDay + 8;
                } else {
                    intShamsMonth = 8;
                    intShamsDay = intDay - 22;
                }
                break;
            case 11:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 8;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 9;
                    intShamsDay = intDay - 21;
                }
                break;
            case 12:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 9;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 10;
                    intShamsDay = intDay - 21;
                }
                break;

        }

        if (intShamsDay < 10) {
            strShamsDay = "0" + String.valueOf(intShamsDay);
        } else {
            strShamsDay = String.valueOf(intShamsDay);
        }
        if (intShamsMonth < 10) {
            strShamsMonth = "0" + String.valueOf(intShamsMonth);
        } else {
            strShamsMonth = String.valueOf(intShamsMonth);
        }
        strShamsYear = String.valueOf(intShamsYear);

        return strShamsYear + "/" + strShamsMonth + "/" + strShamsDay;

    }

    public static String Shamsi(long LongDate) {

        int day, month, year;

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(LongDate);
        Date today = calendar.getTime();


        String dt = formatter.format(today);
//        Log.i("xxx cal", dt);
        String[] arrayDate = dt.split("-");

        day = Integer.valueOf(arrayDate[0]);
//        Log.i("xxx day", day + "");

        month = Integer.valueOf(arrayDate[1]);
//        Log.i("xxx month", month + "");

        year = Integer.valueOf(arrayDate[2]);
//        Log.i("xxx year", year + "");





        intDay = day;
        intMonth = month;
        intYear = year;
		
		if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)) && month >2){
			++intDay;
		}
		
        switch (month) {
            case 1:
                intShamsYear = intYear - 622;
                if (intDay <= 20) {
                    intShamsMonth = 10;
                    intShamsDay = intDay + 10;
                } else {
                    intShamsMonth = 11;
                    intShamsDay = intDay - 20;
                }
                break;
            case 2:
                intShamsYear = intYear - 622;
                if (intDay <= 19) {
                    intShamsMonth = 11;
                    intShamsDay = intDay + 11;
                } else {
                    intShamsMonth = 12;
                    intShamsDay = intDay - 19;
                }
                break;
            case 3:
                if (intDay <= 20) {
                    intShamsMonth = 12;
                    intShamsDay = intDay + 9;
                    intShamsYear = intYear - 622;
                } else {
                    intShamsMonth = 1;
                    intShamsDay = intDay - 20;
                    intShamsYear = intYear - 621;
                }
                break;
            case 4:
                intShamsYear = intYear - 621;
                if (intDay <= 20) {
                    intShamsMonth = 1;
                    intShamsDay = intDay + 11;
                } else {
                    intShamsMonth = 2;
                    intShamsDay = intDay - 20;
                }
                break;
            case 5:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 2;
                    intShamsDay = intDay + 10;
                } else {
                    intShamsMonth = 3;
                    intShamsDay = intDay - 21;
                }
                break;
            case 6:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 3;
                    intShamsDay = intDay + 10;
                } else {
                    intShamsMonth = 4;
                    intShamsDay = intDay - 21;
                }
                break;
            case 7:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 4;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 5;
                    intShamsDay = intDay - 22;
                }
                break;
            case 8:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 5;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 6;
                    intShamsDay = intDay - 22;
                }
                break;
            case 9:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 6;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 7;
                    intShamsDay = intDay - 22;
                }
                break;
            case 10:
                intShamsYear = intYear - 621;
                if (intDay <= 22) {
                    intShamsMonth = 7;
                    intShamsDay = intDay + 8;
                } else {
                    intShamsMonth = 8;
                    intShamsDay = intDay - 22;
                }
                break;
            case 11:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 8;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 9;
                    intShamsDay = intDay - 21;
                }
                break;
            case 12:
                intShamsYear = intYear - 621;
                if (intDay <= 21) {
                    intShamsMonth = 9;
                    intShamsDay = intDay + 9;
                } else {
                    intShamsMonth = 10;
                    intShamsDay = intDay - 21;
                }
                break;

        }

        if (intShamsDay < 10) {
            strShamsDay = "0" + String.valueOf(intShamsDay);
        } else {
            strShamsDay = String.valueOf(intShamsDay);
        }
        if (intShamsMonth < 10) {
            strShamsMonth = "0" + String.valueOf(intShamsMonth);
        } else {
            strShamsMonth = String.valueOf(intShamsMonth);
        }
        strShamsYear = String.valueOf(intShamsYear);

        return strShamsYear + "/" + strShamsMonth + "/" + strShamsDay;

    }

}
