package com.mansouri.wholesalecarpart;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.adaptor.AdapterDetailsOrder;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Detail;
import com.mansouri.wholesalecarpart.model.Order;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ActivityDetailsOrder extends AppCompatActivity implements View.OnClickListener {
    Button btnDetailOrderAccept, btnDetailOrderReject, btnDetailOrderSent;
    ListView list;
    DATASource datasource;
    String order_id, status;
    AdapterDetailsOrder adapterDetailsOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_order);


        initialize();
    }

    private void initialize() {
        order_id = getIntent().getExtras().getString("order_id");
        status = getIntent().getExtras().getString("status");

        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        ImageView imgCall = (ImageView) toolbar.findViewById(R.id.imgToolbarCall);
        imgCall.setVisibility(View.VISIBLE);
        tvTitleToolbar.setText("سفارش از شرکت " + datasource.findUserById(String.valueOf(datasource.findOrderById(order_id).getUser_id())).getCompany());
        Misc.setFont(this, tvTitleToolbar);

        imgCall.setOnClickListener(this);


        btnDetailOrderReject = (Button) findViewById(R.id.btnDetailOrderReject);
        btnDetailOrderAccept = (Button) findViewById(R.id.btnDetailOrderAccept);
        btnDetailOrderSent = (Button) findViewById(R.id.btnDetailOrderSent);
        list = (ListView) findViewById(R.id.listViewDetailOrder);


        btnDetailOrderReject.setOnClickListener(this);
        btnDetailOrderAccept.setOnClickListener(this);
        btnDetailOrderSent.setOnClickListener(this);

        if (status.equals("1")) {
            btnDetailOrderAccept.setVisibility(View.GONE);
            btnDetailOrderReject.setVisibility(View.GONE);
            btnDetailOrderSent.setVisibility(View.GONE);
        } else {
            btnDetailOrderAccept.setVisibility(View.VISIBLE);
            btnDetailOrderReject.setVisibility(View.VISIBLE);
            btnDetailOrderSent.setVisibility(View.VISIBLE);
        }

        Misc.setFont(this, btnDetailOrderAccept);
        Misc.setFont(this, btnDetailOrderReject);
        Misc.setFont(this, btnDetailOrderSent);


        Order order;
        Log.i("xxx order_id", order_id);
        order = datasource.findOrderById(order_id);


        if (status.equals("2")) {
            adapterDetailsOrder = new AdapterDetailsOrder(ActivityDetailsOrder.this, getUnAvailableDetails(order.getMessage(), order.getOrder()), order.getMessage());

            btnDetailOrderReject.setVisibility(View.GONE);
            btnDetailOrderAccept.setVisibility(View.GONE);
            LinearLayout linBottomDetailOrder = (LinearLayout) findViewById(R.id.linBottomDetailOrder);
            linBottomDetailOrder.setWeightSum(1);

        } else {
            adapterDetailsOrder = new AdapterDetailsOrder(ActivityDetailsOrder.this, getDetails(order.getOrder()), order.getMessage());
            Log.i("xxx order", order.toString());
            adapterDetailsOrder.unCheckunAvailableOrder();
            if (order.getAccepted() == 4 || order.getAccepted() == 1) {
                btnDetailOrderReject.setVisibility(View.GONE);
                btnDetailOrderAccept.setVisibility(View.GONE);
                LinearLayout linBottomDetailOrder = (LinearLayout) findViewById(R.id.linBottomDetailOrder);
                linBottomDetailOrder.setWeightSum(1);
            }

        }
        list.setAdapter(adapterDetailsOrder);

    }

    private List<Detail> getUnAvailableDetails(String message, String jsonOrder) {

        List<Detail> details = new ArrayList<>();


        try {
            JSONObject jsonCount = new JSONObject(jsonOrder);
            JSONArray json = new JSONArray(message);
            for (int i = 0; i < json.length(); i++) {
                Detail detail = new Detail();
                detail.setId(datasource.findProductById(String.valueOf(json.getInt(i))).getId());
                detail.setName(datasource.findProductById(String.valueOf(json.getInt(i))).getName());

                detail.setCount(Integer.parseInt(jsonCount.getString(json.get(i).toString())));

                detail.setBrand(datasource.findBrandById(datasource.findProductById(String.valueOf(json.get(i))).getBrand_id()).getName());
                detail.setShareCar(datasource.findProductById(String.valueOf(json.get(i))).getShare_car());
                details.add(detail);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return details;
    }

    private List<Detail> getDetails(String jsonOrder) {
        List<Detail> details = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(jsonOrder);
            Log.i("xxx jsonObject", json.toString());


            Iterator iterator = json.keys();
            while (iterator.hasNext()) {

                Detail detail = new Detail();
                String pid = String.valueOf(iterator.next());
                String count = json.getString(pid);

                detail.setId(Integer.parseInt(pid));
                detail.setName(datasource.findProductById(pid).getName());
                detail.setCount(Integer.parseInt(count));
                detail.setBrand(datasource.findBrandById(datasource.findProductById(pid).getBrand_id()).getName());
                detail.setShareCar(datasource.findProductById(pid).getShare_car());

                details.add(detail);

            }


        } catch (JSONException e) {

        }


        return details;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDetailOrderReject:
                new SweetAlertDialog(ActivityDetailsOrder.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("پیغام")
                        .setContentText("آیا مایل به رد سفارش می باشید؟")
                        .setConfirmText("بله")
                        .setCancelText("خیر")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                                setOrderRejectOrAccept(ActivityDetailsOrder.this, order_id, 2);

                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
//                                finish();
                            }
                        })
                        .show();

                break;
            case R.id.btnDetailOrderAccept:
                new SweetAlertDialog(ActivityDetailsOrder.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("پیغام")
                        .setContentText("آیا مایل به قبول سفارش می باشید؟")
                        .setConfirmText("بله")
                        .setCancelText("خیر")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();

                                if (adapterDetailsOrder.getunAvailableOrderNames().equals("")) {
                                    setOrderRejectOrAccept(ActivityDetailsOrder.this, order_id, 1);
                                } else {
                                    setOrderRejectOrAccept(ActivityDetailsOrder.this, order_id, 4);
                                }


                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
//                                finish();
                            }
                        })
                        .show();
                break;

            case R.id.btnDetailOrderSent:
                setOrderRejectOrAccept(ActivityDetailsOrder.this, order_id, 3);
                break;

            case R.id.imgToolbarCall:
                String uri = "tel:"+ datasource.findUserById(String.valueOf(datasource.findOrderById(order_id).getUser_id())).getMobile();
                Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(uri));
                startActivity(dialIntent);

                break;
        }
    }


    public void setOrderRejectOrAccept(final Context context, final String order_id, final int status) {
        String URL = Misc.Server_Url() + "accept_reject.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();

                        Order order;
                        order = datasource.findOrderById(order_id);
                        order.setAccepted(status);
                        order.setMessage(adapterDetailsOrder.getunAvailableOrderNames());

                        Log.i("xxx order123", order.toString());


                        datasource.createOrUpdateOrder(order);


                        if (order.getAccepted() == 4 || order.getAccepted() == 1) {
                            btnDetailOrderReject.setVisibility(View.GONE);
                            btnDetailOrderAccept.setVisibility(View.GONE);
                            LinearLayout linBottomDetailOrder = (LinearLayout) findViewById(R.id.linBottomDetailOrder);
                            linBottomDetailOrder.setWeightSum(1);
                        }


//                        datasource.updateOrder(Integer.parseInt(order_id), status, adapterDetailsOrder.getunAvailableOrderNames());

                        if (status == 2 || status == 3) {
                            finish();
                        }


                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        Misc.setDialog(context, "خطا", "دوباره سعی کنید", 'e');

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("order_id", String.valueOf(order_id));
                params.put("status", String.valueOf(status));
                params.put("message", adapterDetailsOrder.getunAvailableOrderNames());

                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }


}
