package com.mansouri.wholesalecarpart.model;

/**
 * Created by Hossein on 2/22/2016.
 */
public class Detail {
    int id;
    int count;
    String name;
    int tag;
    String Brand;
    String ShareCar;

    public Detail() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getShareCar() {
        return ShareCar;
    }

    public void setShareCar(String shareCar) {
        ShareCar = shareCar;
    }

}
