package com.mansouri.wholesalecarpart.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class databaseOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "carpart.db";
    private static final int DATABASE_VERSION = 3;

    private static final String TABLE_CREATE_USERS = "CREATE TABLE `users` (" +
            "`id` INTEGER," +
            "`fullname` TEXT," +
            "`company` TEXT," +
            "`address` TEXT," +
            "`mobile` TEXT," +
            "`landline` TEXT," +
            "`mellicard` TEXT," +
            "`active` INTEGER," +
            "`password` TEXT);";

    private static final String TABLE_CREATE_CARS = "CREATE TABLE `cars` (" +
            "`id` INTEGER," +
            "`name` TEXT);";

    private static final String TABLE_CREATE_BRANDS = "CREATE TABLE `brands` (" +
            "`id` INTEGER," +
            "`name` TEXT);";

    private static final String TABLE_CREATE_CATEGORIES = "CREATE TABLE `categories` (" +
            "`id` INTEGER," +
            "`photo` INTEGER," +
            "`name` TEXT);";

    private static final String TABLE_CREATE_PRODUCTS = "CREATE TABLE `products` (" +
            "`id` INTEGER," +
            "`category_id` INTEGER," +
            "`name` TEXT," +
            "`sharecar` TEXT," +
            "`price` INTEGER," +
            "`description` TEXT," +
            "`priority` INTEGER," +
            "`photos` INTEGER," +
            "`garanty` TEXT," +
            "`box_size` TEXT," +
            "`brand_id` INTEGER," +
            "`active` INTEGER," +
            "`favorite` INTEGER);";

    private static final String TABLE_CREATE_ORDERS = "CREATE TABLE `orders` (" +
            "`id` INTEGER," +
            "`user_id` INTEGER," +
            "`order_time` TEXT," +
            "`orders` TEXT," +
            "`accepted` INTEGER," +
            "`message` TEXT," +
            "`description` TEXT );";

    public databaseOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE_USERS);
        db.execSQL(TABLE_CREATE_CARS);
        db.execSQL(TABLE_CREATE_BRANDS);
        db.execSQL(TABLE_CREATE_CATEGORIES);
        db.execSQL(TABLE_CREATE_PRODUCTS);
        db.execSQL(TABLE_CREATE_ORDERS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + "users");
        db.execSQL("DROP TABLE IF EXISTS " + "cars");
        db.execSQL("DROP TABLE IF EXISTS " + "brands");
        db.execSQL("DROP TABLE IF EXISTS " + "categories");
        db.execSQL("DROP TABLE IF EXISTS " + "products");
        db.execSQL("DROP TABLE IF EXISTS " + "orders");

        onCreate(db);
    }

}
