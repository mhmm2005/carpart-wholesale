package com.mansouri.wholesalecarpart;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.library.PersianDate;
import com.mansouri.wholesalecarpart.model.Car;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ActivityRegisterCar extends AppCompatActivity implements View.OnClickListener {

    EditText etCarName;
    Button btnRegisterCar;
    DATASource datasource;
    Car car = new Car();
    String car_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_car);


        initialize();
    }

    private void initialize() {
        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("ثبت خودرو");
        Misc.setFont(this, tvTitleToolbar);

        etCarName = (EditText) findViewById(R.id.etCarName);
        btnRegisterCar = (Button) findViewById(R.id.btnRegisterCar);

        Misc.setFont(this, etCarName,25,false);
        Misc.setFont(this, btnRegisterCar,18,true);

        btnRegisterCar.setOnClickListener(this);
        try {
            car_id = getIntent().getExtras().getString("car_id");
            car = datasource.findCarById(car_id);
            etCarName.setText(car.getName());

        } catch (Exception e) {

        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegisterCar:
                if (etCarName.getText().toString().trim().equals("")) {
                    RelativeLayout rel = (RelativeLayout) findViewById(R.id.relcar);
                    Misc.snack(ActivityRegisterCar.this, rel, "لطفا تمامی فیلد ها را پر نمایید");
                } else {

                    if (car_id.equals("")) {
                        car.setId(-1);
                    }

                    car.setName(etCarName.getText().toString());

                    registerCarRequest(ActivityRegisterCar.this, car);
                }
                break;
        }
    }


    public void registerCarRequest(final Context context, final Car car) {
        String URL = Misc.Server_Url() + "register_car.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();
                        Log.i("xxx time", PersianDate.Shamsi(obj.getLong("time")));

                        String car_id = obj.getString("car_id");
                        car.setId(Integer.parseInt(car_id));
                        datasource.saveOrUpdateCar(car);

                        new SweetAlertDialog(ActivityRegisterCar.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("پیام سیستم")
                                .setContentText("اطلاعات با موفقیت ثبت گردید")
                                .setConfirmText("خب")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        finish();
                                    }
                                })
                                .show();

                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        Misc.setDialog(context, "خطا", "اطلاعات اشتباه", 'e');
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(car.getId()));
                params.put("name", car.getName());
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }

}
