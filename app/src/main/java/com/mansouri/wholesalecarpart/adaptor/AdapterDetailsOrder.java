package com.mansouri.wholesalecarpart.adaptor;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.R;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Detail;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;


public class AdapterDetailsOrder extends BaseAdapter {

    private static LayoutInflater inflater = null;
    DATASource datasource;
    String unAvailable;
    JSONArray json;
    private Activity activity;
    private List<Detail> data;

    public AdapterDetailsOrder(Activity a, List<Detail> d, String message) {
        activity = a;
        data = d;
        datasource = new DATASource(activity);

        if (message == null) {
            unAvailable = "";
        } else {
            unAvailable = message;
        }

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < data.size(); i++) {
            data.get(i).setTag(1);
        }
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public String getunAvailableOrderNames() {
        String unAvailable = "[";
        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getTag() == 0) {
                    unAvailable += data.get(i).getId() + ",";
                }
            }
            unAvailable = unAvailable.substring(0, unAvailable.length() - 1);
            unAvailable += "]";

            if (unAvailable.equals("]")) {
                unAvailable = "";
            }
        }
        Log.i("xxx unAvailable", unAvailable);
        return unAvailable.trim();
    }

    public void unCheckunAvailableOrder() {
        try {
            json = new JSONArray(unAvailable);
            for (int i = 0; i < data.size(); i++) {
                for (int j = 0; j < json.length(); j++) {
                    if (json.get(j).equals(data.get(i).getId())) {
                        data.get(i).setTag(0);
                        Log.i("xxx unCheckunAvailable", data.get(i).getName());
                    }
                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.row_details_order, null);
        }

        TextView tvOrderProductId = (TextView) vi.findViewById(R.id.tvRowDetailOrderProductId);
        TextView tvOrderProductName = (TextView) vi.findViewById(R.id.tvRowDetailOrderProductName);
        TextView tvOrderProductQuantity = (TextView) vi.findViewById(R.id.tvRowDetailOrderProductQuantity);
        TextView tvOrderProductBrand = (TextView) vi.findViewById(R.id.tvRowDetailOrderProductBrand);
        TextView tvOrderProductShareCar = (TextView) vi.findViewById(R.id.tvRowDetailOrderProductShareCar);
        final CheckBox chkProductOrder = (CheckBox) vi.findViewById(R.id.chkRowDetailsOrder);

        Misc.setFont(activity, tvOrderProductName, 16, true);
        Misc.setFont(activity, tvOrderProductQuantity, 14, true);
        Misc.setFont(activity, tvOrderProductBrand, 14, true);
        Misc.setFont(activity, tvOrderProductShareCar, 14, true);

        Detail detail = data.get(position);

        tvOrderProductId.setText(String.valueOf(detail.getId()));
        tvOrderProductName.setText(detail.getName());
        tvOrderProductQuantity.setText("تعداد: " + String.valueOf(detail.getCount()) + " عدد ");
        tvOrderProductBrand.setText("مارک: " + detail.getBrand());
        tvOrderProductShareCar.setText("مورد استفاده ی: " + detail.getShareCar());

        if (data.get(position).getTag() == 1) {
            chkProductOrder.setChecked(true);
        } else {
            chkProductOrder.setChecked(false);
        }

        chkProductOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkProductOrder.isChecked()) {
                    chkProductOrder.setChecked(true);
                    data.get(position).setTag(1);
//                    Log.i("xxx position-chk",position + " " + chkProductOrder.getText().toString());
                } else {
                    chkProductOrder.setChecked(false);
                    data.get(position).setTag(0);
//                    Log.i("xxx e position-chk", position + " " + chkProductOrder.getText().toString());
                }
            }
        });

        return vi;
    }

}