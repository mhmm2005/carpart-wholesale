package com.mansouri.wholesalecarpart;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.adaptor.AdapterBrand;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;

public class ActivityBrand extends AppCompatActivity implements View.OnClickListener {

    FloatingActionButton btnNewBrand;
    ListView list;
    DATASource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand);


        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        list.setAdapter(new AdapterBrand(this, datasource.getAllBrands()));
    }

    private void initialize() {

        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("مارک");
        Misc.setFont(this, tvTitleToolbar);

        btnNewBrand = (FloatingActionButton) findViewById(R.id.btnFabCreateNewBrand);
        btnNewBrand.setOnClickListener(this);

        list = (ListView) findViewById(R.id.listViewBrand);
//        list.setAdapter(new AdapterCar(this, datasource.getAllCars()));


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tvId = (TextView) view.findViewById(R.id.tvRowBrandId);
                Log.i("xxx brand id", tvId.getText().toString());
                Intent intent = new Intent(ActivityBrand.this, ActivityRegisterBrands.class);
                intent.putExtra("brand_id", tvId.getText().toString());

                startActivity(intent);

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFabCreateNewBrand:
                startActivity(new Intent(ActivityBrand.this, ActivityRegisterBrands.class));

                break;
        }
    }

}
