package com.mansouri.wholesalecarpart.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mansouri.wholesalecarpart.model.Brand;
import com.mansouri.wholesalecarpart.model.Car;
import com.mansouri.wholesalecarpart.model.Category;
import com.mansouri.wholesalecarpart.model.Order;
import com.mansouri.wholesalecarpart.model.Product;
import com.mansouri.wholesalecarpart.model.User;

import java.util.ArrayList;
import java.util.List;

public class DATASource {

    private static final String[] getAllColumnsUsers = {
            "id",
            "fullname",
            "company",
            "address",
            "mobile",
            "landline",
            "mellicard",
            "active",
            "password"
    };

    private static final String[] allColumnsCars = {
            "id",
            "name"};

    private static final String[] allColumnsBrands = {
            "id",
            "name"};

    private static final String[] allColumnsCategories = {
            "id",
            "photo",
            "name"};

    private static final String[] allColumnsOrders = {
            "id",
            "user_id",
            "order_time",
            "orders",
            "accepted",
            "message",
            "description"};

    private static final String[] allColumnsProducts = {
            "id",
            "category_id",
            "name",
            "sharecar",
            "price",
            "description",
            "priority",
            "photos",
            "garanty",
            "box_size",
            "brand_id",
            "active",
            "favorite"};

    SQLiteOpenHelper dbhelper;
    SQLiteDatabase database;

    public DATASource(Context context) {
        dbhelper = new databaseOpenHelper(context);
        database = dbhelper.getWritableDatabase();
    }


    public String getCompanyName(int user_id) {
        Cursor cursor = database.query("users", getAllColumnsUsers, "id=" + user_id, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            return cursor.getString(cursor.getColumnIndex("company"));
        }
        return null;
    }

    public List<Car> getAllCars() {
        List<Car> cars = new ArrayList<>();
        Cursor cursor = database.query("cars", allColumnsCars, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            Car car;
            while (cursor.moveToNext()) {
                car = new Car();
                car.setId(cursor.getInt(cursor.getColumnIndex("id")));
                car.setName(cursor.getString(cursor.getColumnIndex("name")));
                cars.add(car);
            }
        }
        return cars;
    }

    public List<Category> getAllCategories() {
        List<Category> categories = new ArrayList<>();
        Cursor cursor = database.query("categories", allColumnsCategories, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            Category category;
            while (cursor.moveToNext()) {
                category = new Category();
                category.setId(cursor.getInt(cursor.getColumnIndex("id")));
                category.setName(cursor.getString(cursor.getColumnIndex("name")));
                category.setPhoto(cursor.getInt(cursor.getColumnIndex("photo")));
                categories.add(category);
            }
        }
        return categories;
    }

    public List<User> getAllUsers(int active) {
        List<User> users = new ArrayList<>();
        Cursor cursor = database.query("users", getAllColumnsUsers, "active=" + active, null, null, null, null);
        if (cursor.getCount() > 0) {
            User user;
            while (cursor.moveToNext()) {
                user = new User();
                user.setId(cursor.getInt(cursor.getColumnIndex("id")));
                user.setFullName(cursor.getString(cursor.getColumnIndex("fullname")));
                user.setCompany(cursor.getString(cursor.getColumnIndex("company")));
                user.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                user.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
                user.setLandLine(cursor.getString(cursor.getColumnIndex("landline")));
                user.setMelliCard(cursor.getString(cursor.getColumnIndex("mellicard")));
                user.setActive(cursor.getInt(cursor.getColumnIndex("active")));
                user.setPassword(cursor.getString(cursor.getColumnIndex("password")));
                users.add(user);
            }
        }
        return users;
    }

    public List<Product> getAllProducts(String category_id) {

        List<Product> products = new ArrayList<>();
        Cursor cursor = database.query("products", allColumnsProducts, "category_id=" + category_id + " AND active = 1", null, null, null, null);
        if (cursor.getCount() > 0) {
            Product product;
            while (cursor.moveToNext()) {
                product = new Product();

                product.setId(cursor.getInt(cursor.getColumnIndex("id")));
                product.setCategory_id(cursor.getInt(cursor.getColumnIndex("category_id")));
                product.setName(cursor.getString(cursor.getColumnIndex("name")));
                product.setShare_car(cursor.getString(cursor.getColumnIndex("sharecar")));
                product.setPrice(cursor.getInt(cursor.getColumnIndex("price")));
                product.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                product.setPriority(cursor.getInt(cursor.getColumnIndex("priority")));
                product.setPhoto(cursor.getInt(cursor.getColumnIndex("photos")));
                product.setGaranty(cursor.getString(cursor.getColumnIndex("garanty")));
                product.setBox_size(cursor.getString(cursor.getColumnIndex("box_size")));
                product.setBrand_id(cursor.getInt(cursor.getColumnIndex("brand_id")));
                product.setFavorite(cursor.getInt(cursor.getColumnIndex("favorite")));
                products.add(product);
            }
        }
        return products;
    }

    public void saveOrUpdateUser(User user) {
        ContentValues values = new ContentValues();

        values.put("id", user.getId());
        values.put("fullname", user.getFullName());
        values.put("company", user.getCompany());
        values.put("address", user.getAddress());
        values.put("mobile", user.getMobile());
        values.put("landline", user.getLandLine());
        values.put("mellicard", user.getMelliCard());
        values.put("active", user.getActive());
        values.put("password", user.getPassword());

        Cursor cursor = database.query("users", getAllColumnsUsers, "id=" + user.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("users", values, "id=" + user.getId(), null);
            cursor.close();
        } else {
            database.insert("users", null, values);
            cursor.close();
        }

    }

    public User findUserById(String id) {
        User user = new User();
        Cursor cursor = database.query("users", getAllColumnsUsers, "id=" + id, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            user.setId(cursor.getInt(cursor.getColumnIndex("id")));
            user.setFullName(cursor.getString(cursor.getColumnIndex("fullname")));
            user.setCompany(cursor.getString(cursor.getColumnIndex("company")));
            user.setAddress(cursor.getString(cursor.getColumnIndex("address")));
            user.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
            user.setLandLine(cursor.getString(cursor.getColumnIndex("landline")));
            user.setMelliCard(cursor.getString(cursor.getColumnIndex("mellicard")));
            user.setActive(cursor.getInt(cursor.getColumnIndex("active")));
            user.setPassword(cursor.getString(cursor.getColumnIndex("password")));

        }
        return user;
    }

    public void deactiveUser(String user_id) {
        ContentValues values = new ContentValues();

        values.put("active", "0");

        database.update("users", values, "id=" + user_id, null);

    }

    public void activeUser(String user_id) {
        ContentValues values = new ContentValues();

        values.put("active", "1");

        database.update("users", values, "id=" + user_id, null);

    }

    public Car findCarById(String id) {
        Car car = new Car();
        Cursor cursor = database.query("cars", allColumnsCars, "id=" + id, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            car.setId(cursor.getInt(cursor.getColumnIndex("id")));
            car.setName(cursor.getString(cursor.getColumnIndex("name")));

        }

        return car;
    }

    public void saveOrUpdateCar(Car car) {
        ContentValues values = new ContentValues();
        values.put("id", car.getId());
        values.put("name", car.getName());

        Cursor cursor = database.query("cars", allColumnsCars, "id=" + car.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("cars", values, "id=" + car.getId(), null);
            cursor.close();
        } else {
            database.insert("cars", null, values);
            cursor.close();
        }
    }

    public void deleteCar(String car_id) {
        database.delete("cars", "id=" + car_id, null);
    }

    public Category findCategoryById(String id) {
        Category category = new Category();
        Cursor cursor = database.query("categories", allColumnsCategories, "id=" + id, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            category.setId(cursor.getInt(cursor.getColumnIndex("id")));
            category.setName(cursor.getString(cursor.getColumnIndex("name")));
            category.setPhoto(cursor.getInt(cursor.getColumnIndex("photo")));

        }

        return category;
    }

    public void saveOrUpdateCategory(Category category) {
        ContentValues values = new ContentValues();
        values.put("id", category.getId());
        values.put("name", category.getName());
        values.put("photo", category.getId());

        Cursor cursor = database.query("categories", allColumnsCategories, "id=" + category.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("categories", values, "id=" + category.getId(), null);
            cursor.close();
        } else {
            database.insert("categories", null, values);
            cursor.close();
        }
    }

    public Product findProductById(String id) {

        Product product = new Product();
        Cursor cursor = database.query("products", allColumnsProducts, "id=" + id, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            product.setId(cursor.getInt(cursor.getColumnIndex("id")));
            product.setCategory_id(cursor.getInt(cursor.getColumnIndex("category_id")));
            product.setName(cursor.getString(cursor.getColumnIndex("name")));
            product.setShare_car(cursor.getString(cursor.getColumnIndex("sharecar")));
            product.setPrice(cursor.getInt(cursor.getColumnIndex("price")));
            product.setDescription(cursor.getString(cursor.getColumnIndex("description")));
            product.setPriority(cursor.getInt(cursor.getColumnIndex("priority")));
            product.setPhoto(cursor.getInt(cursor.getColumnIndex("photos")));
            product.setGaranty(cursor.getString(cursor.getColumnIndex("garanty")));
            product.setBox_size(cursor.getString(cursor.getColumnIndex("box_size")));
            product.setBrand_id(cursor.getInt(cursor.getColumnIndex("brand_id")));
            product.setFavorite(cursor.getInt(cursor.getColumnIndex("favorite")));

        }

        return product;

    }

    public Brand findBrandById(int id) {
        Brand brand = new Brand();
        Cursor cursor = database.query("brands", allColumnsBrands, "id=" + id, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            brand.setId(cursor.getInt(cursor.getColumnIndex("id")));
            brand.setName(cursor.getString(cursor.getColumnIndex("name")));
        }
        return brand;
    }

    public int findBrandIdByName(String name) {
        int id = 0;
        Cursor cursor = database.query("brands", allColumnsBrands, "name='" + name + "'", null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            id = cursor.getInt(cursor.getColumnIndex("id"));

        }
        return id;

    }

    public void deleteProduct(String id) {
        ContentValues values = new ContentValues();
        values.put("active", "0");

//        database.delete("products", "id=" + id, null);

        database.update("products", values, "id=" + id, null);

    }

    public List<Brand> getAllBrands() {

        List<Brand> brands = new ArrayList<>();
        Cursor cursor = database.query("brands", allColumnsBrands, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            Brand brand;
            while (cursor.moveToNext()) {
                brand = new Brand();
                brand.setId(cursor.getInt(cursor.getColumnIndex("id")));
                brand.setName(cursor.getString(cursor.getColumnIndex("name")));
                brands.add(brand);
            }
        }
        return brands;
    }

    public void createOrUpdateBrand(Brand brand) {
        ContentValues values = new ContentValues();
        values.put("id", brand.getId());
        values.put("name", brand.getName());

        Cursor cursor = database.query("brands", allColumnsBrands, "id=" + brand.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("brands", values, "id=" + brand.getId(), null);
            cursor.close();
        } else {
            database.insert("brands", null, values);
            cursor.close();
        }
    }

    public void createOrUpdateOrder(Order order) {
        ContentValues values = new ContentValues();

        values.put("id", order.getId());
        values.put("user_id", order.getUser_id());
        values.put("order_time", order.getOrder_time());
        values.put("orders", order.getOrder());
        values.put("accepted", order.getAccepted());
        values.put("message", order.getMessage());
        values.put("description", order.getDescription());

        Cursor cursor = database.query("orders", allColumnsOrders, "id=" + order.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("orders", values, "id=" + order.getId(), null);
            cursor.close();
        } else {
            database.insert("orders", null, values);
            cursor.close();
        }

    }

    public Order findOrderById(String id) {
        Order order = new Order();
        Cursor cursor = database.query("orders", allColumnsOrders, "id=" + id, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            order.setId(cursor.getInt(cursor.getColumnIndex("id")));
            order.setUser_id(cursor.getInt(cursor.getColumnIndex("user_id")));
            order.setOrder_time(cursor.getLong(cursor.getColumnIndex("order_time")));
            order.setOrder(cursor.getString(cursor.getColumnIndex("orders")));
            order.setAccepted(cursor.getInt(cursor.getColumnIndex("accepted")));
            order.setMessage(cursor.getString(cursor.getColumnIndex("message")));
            order.setDescription(cursor.getString(cursor.getColumnIndex("description")));
        }
        return order;
    }

    public List<Order> getAllOrders() {
        List<Order> orders = new ArrayList<>();
        Cursor cursor = database.query("orders", allColumnsOrders, "accepted = 0 OR accepted = 1 OR accepted = 4", null, null, null, "order_time DESC");
        if (cursor.getCount() > 0) {
            Order order;
            while (cursor.moveToNext()) {
                order = new Order();
                order.setId(cursor.getInt(cursor.getColumnIndex("id")));
                order.setUser_id(cursor.getInt(cursor.getColumnIndex("user_id")));
                order.setOrder(cursor.getString(cursor.getColumnIndex("orders")));
                order.setOrder_time(cursor.getLong(cursor.getColumnIndex("order_time")));
                order.setAccepted(cursor.getInt(cursor.getColumnIndex("accepted")));
                order.setMessage(cursor.getString(cursor.getColumnIndex("message")));
                order.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                orders.add(order);
            }
        }
        return orders;
    }

    public List<Order> getAllArchives() {
        List<Order> orders = new ArrayList<>();
        Cursor cursor = database.query("orders", allColumnsOrders, "accepted = 2 OR (accepted = 3 AND LENGTH(message) = 0)", null, null, null, "order_time DESC");
        if (cursor.getCount() > 0) {
            Order order;
            while (cursor.moveToNext()) {
                order = new Order();
                order.setId(cursor.getInt(cursor.getColumnIndex("id")));
                order.setUser_id(cursor.getInt(cursor.getColumnIndex("user_id")));
                order.setOrder(cursor.getString(cursor.getColumnIndex("orders")));
                order.setOrder_time(cursor.getLong(cursor.getColumnIndex("order_time")));
                order.setAccepted(cursor.getInt(cursor.getColumnIndex("accepted")));
                order.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                orders.add(order);
            }
        }
        return orders;
    }

    public List<Order> getAllInCompleteOrders() {
        List<Order> orders = new ArrayList<>();
        Cursor cursor = database.query("orders", allColumnsOrders, "accepted = 3 AND LENGTH(message) > 0", null, null, null, "order_time DESC");
        if (cursor.getCount() > 0) {
            Order order;
            while (cursor.moveToNext()) {
                order = new Order();
                order.setId(cursor.getInt(cursor.getColumnIndex("id")));
                order.setUser_id(cursor.getInt(cursor.getColumnIndex("user_id")));
                order.setOrder(cursor.getString(cursor.getColumnIndex("orders")));
                order.setOrder_time(cursor.getLong(cursor.getColumnIndex("order_time")));
                order.setAccepted(cursor.getInt(cursor.getColumnIndex("accepted")));
                order.setMessage(cursor.getString(cursor.getColumnIndex("message")));
                order.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                orders.add(order);
            }
        }
        return orders;

    }

    public void createOrUpdateCategory(Category category) {
        ContentValues values = new ContentValues();

        values.put("id", category.getId());
        values.put("name", category.getName());
        values.put("photo", category.getId());

        Cursor cursor = database.query("categories", allColumnsCategories, "id=" + category.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("categories", values, "id=" + category.getId(), null);
            cursor.close();
        } else {
            database.insert("categories", null, values);
            cursor.close();
        }
    }

    public void createOrUpdateCar(Car car) {
        ContentValues values = new ContentValues();

        values.put("id", car.getId());
        values.put("name", car.getName());

        Cursor cursor = database.query("cars", allColumnsCars, "id=" + car.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("cars", values, "id=" + car.getId(), null);
            cursor.close();
        } else {
            database.insert("cars", null, values);
            cursor.close();
        }
    }

    public void createOrUpdateCompany(Brand brand) {
        ContentValues values = new ContentValues();

        values.put("id", brand.getId());
        values.put("name", brand.getName());

        Cursor cursor = database.query("brands", allColumnsBrands, "id=" + brand.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("brands", values, "id=" + brand.getId(), null);
            cursor.close();
        } else {
            database.insert("brands", null, values);
            cursor.close();
        }
    }

    public void createOrUpdateUser(User user) {
        ContentValues values = new ContentValues();

        values.put("id", user.getId());
        values.put("fullname", user.getFullName());
        values.put("company", user.getCompany());
        values.put("mellicard", user.getMelliCard());
        values.put("landline", user.getLandLine());
        values.put("mobile", user.getMobile());
        values.put("address", user.getAddress());
        values.put("password", user.getPassword());
        values.put("active", user.getActive());

        Cursor cursor = database.query("users", getAllColumnsUsers, "id=" + user.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("users", values, "id=" + user.getId(), null);
            cursor.close();
        } else {
            database.insert("users", null, values);
            cursor.close();
        }
    }

    public void createOrUpdateProduct(Product product) {
        ContentValues values = new ContentValues();

        values.put("id", product.getId());
        values.put("category_id", product.getCategory_id());
        values.put("name", product.getName());
        values.put("sharecar", product.getShare_car());
        values.put("price", product.getPrice());
        values.put("description", product.getDescription());
        values.put("photos", product.getPhoto());
        values.put("garanty", product.getGaranty());
        values.put("box_size", product.getBox_size());
        values.put("brand_id", product.getBrand_id());
        values.put("favorite", product.getFavorite());
        values.put("active", product.getActive());
        values.put("priority", product.getPriority());

        Cursor cursor = database.query("products", allColumnsProducts, "id=" + product.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("products", values, "id=" + product.getId(), null);
            cursor.close();
        } else {
            database.insert("products", null, values);
            cursor.close();
        }

    }

    public void saveOrUpdateProduct(Product product) {

        ContentValues values = new ContentValues();

        values.put("id", product.getId());
        values.put("name", product.getName());
        values.put("photos", product.getPhoto());
        values.put("brand_id", product.getBrand_id());
        values.put("garanty", product.getGaranty());
        values.put("description", product.getDescription());
        values.put("box_size", product.getBox_size());
        values.put("sharecar", product.getShare_car());
        values.put("active", product.getActive());
        values.put("category_id", product.getCategory_id());
        values.put("price", String.valueOf(product.getPrice()));

        Cursor cursor = database.query("products", allColumnsProducts, "id=" + product.getId(), null, null, null, null);
        if (cursor.getCount() > 0) {
            database.update("products", values, "id=" + product.getId(), null);
            cursor.close();
        } else {
            database.insert("products", null, values);
            cursor.close();
        }


    }

    public List<Car> AllCars() {
        List<Car> cars = new ArrayList<>();
        Cursor cursor = database.query("cars", allColumnsCars, null, null, null, null, "name ASC");
        if (cursor.getCount() > 0) {
            Car car;


            while (cursor.moveToNext()) {
                car = new Car();
                car.setId(cursor.getInt(cursor.getColumnIndex("id")));
                car.setName(cursor.getString(cursor.getColumnIndex("name")));
                cars.add(car);
            }

            car = new Car();
            car.setId(0);
            car.setName(" انتخاب خودرو");
            cars.add(car);
        }
        return cars;
    }

    public List<Brand> AllCompanies() {
        List<Brand> companies = new ArrayList<>();
        Cursor cursor = database.query("brands", allColumnsBrands, null, null, null, null, "name ASC");
        if (cursor.getCount() > 0) {
            Brand company;


            while (cursor.moveToNext()) {
                company = new Brand();
                company.setId(cursor.getInt(cursor.getColumnIndex("id")));
                company.setName(cursor.getString(cursor.getColumnIndex("name")));
                companies.add(company);
            }

            company = new Brand();
            company.setId(0);
            company.setName("  شرکت سازنده");
            companies.add(company);
        }
        return companies;
    }

    public List<Product> searchAdvanceProducts(String strName, String strCompanyId, String strCarName, int category_id) {
        String strSql;
        String strWhere;
        String strTable;
        if (strCompanyId.equals("0")) {
            if (strCarName.equals(" انتخاب خودرو")) {
                strWhere = " where products.category_id=" + category_id + " AND products.name like '%" + strName + "%'";
            } else {
                strWhere = " where products.category_id=" + category_id + " AND products.name like '%" + strName + "%' AND products.sharecar like '%" + strCarName + "%'";
            }
        } else {
            if (strCarName.equals(" انتخاب خودرو")) {
                strWhere = " where products.category_id=" + category_id + " AND products.name like '%" + strName + "%' AND products.brand_id =" + strCompanyId;
            } else {
                strWhere = " where products.category_id=" + category_id + " AND products.name like '%" + strName + "%' AND products.brand_id =" + strCompanyId + " AND products.sharecar like '%" + strCarName + "%'";
            }
        }


        strTable = "select * from products" + strWhere + " AND active=1";
        strSql = strTable;

        Log.i("xxx strTotal", strSql);
        List<Product> products = new ArrayList<>();
        Cursor cursor = database.rawQuery(strSql, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Product product = new Product();
                product.setId(cursor.getInt(cursor.getColumnIndex("id")));
                product.setCategory_id(cursor.getInt(cursor.getColumnIndex("category_id")));
                product.setName(cursor.getString(cursor.getColumnIndex("name")));
                product.setShare_car(cursor.getString(cursor.getColumnIndex("sharecar")));
                product.setPrice(cursor.getInt(cursor.getColumnIndex("price")));
                product.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                product.setPhoto(cursor.getInt(cursor.getColumnIndex("photos")));
                product.setGaranty(cursor.getString(cursor.getColumnIndex("garanty")));
                product.setBox_size(cursor.getString(cursor.getColumnIndex("box_size")));
                product.setBrand_id(cursor.getInt(cursor.getColumnIndex("brand_id")));
                product.setFavorite(cursor.getInt(cursor.getColumnIndex("favorite")));
                product.setPriority(cursor.getInt(cursor.getColumnIndex("priority")));
                products.add(product);
            }
        }
        return products;
    }

    public void deleteAllCategories() {
        database.delete("categories",null, null);

    }

    public void deleteAllCompanies() {
        database.delete("brands",null, null);

    }

    public void deleteAllCars() {
        database.delete("cars",null, null);

    }

    public void deleteAllUsers() {
        database.delete("users",null, null);

    }

    public void deleteAllProducts() {
        database.delete("products",null, null);

    }

    public void deleteAllOrders() {
        database.delete("orders",null, null);

    }
}
