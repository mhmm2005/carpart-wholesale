package com.mansouri.wholesalecarpart.adaptor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.ActivityRegisterCategory;
import com.mansouri.wholesalecarpart.R;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Category;
import com.squareup.picasso.Picasso;

import java.util.List;


public class AdapterCategory extends BaseAdapter {

    private static LayoutInflater inflater = null;
    DATASource datasource;
    private Activity activity;
    private List<Category> data;

    public AdapterCategory(Activity a, List<Category> d) {
        activity = a;
        data = d;
        datasource = new DATASource(activity);
//        datasource.open();

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.row_category, null);
        }

        TextView tvCategoryId = (TextView) vi.findViewById(R.id.tvRowCategoryId);
        TextView tvCategoryName = (TextView) vi.findViewById(R.id.tvRowCategoryName);
//        CircleImageView imgCategory = (CircleImageView) vi.findViewById(R.id.imgRowCategory);
        ImageView imgCategory = (ImageView) vi.findViewById(R.id.imgRowCategory);
        ImageView imgEdit = (ImageView) vi.findViewById(R.id.imgRowCategoryEdit);

        Misc.setFont(activity, tvCategoryName, 16, true);

        final Category category = data.get(position);

        tvCategoryId.setText(String.valueOf(category.getId()));
        tvCategoryName.setText(category.getName());
//        Random rnd = new Random();

        Picasso.with(activity)
                .load(Misc.Server_Url() + "image/cat" + category.getPhoto() + ".png?random=" + Misc.getLastUpdateCategory(activity))
                .noFade()
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(imgCategory);

        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("xxx cat id adap", String.valueOf(category.getId()));

                Intent intent = new Intent(activity, ActivityRegisterCategory.class);
                intent.putExtra("category_id", String.valueOf(category.getId()));

                activity.startActivity(intent);
            }
        });

        return vi;
    }

}