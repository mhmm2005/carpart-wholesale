package com.mansouri.wholesalecarpart;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.adaptor.AdapterCar;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivityCar extends AppCompatActivity implements View.OnClickListener, AdapterCar.ComponentClickListener {

    FloatingActionButton btnNewCar;
    ListView list;
    DATASource datasource;
    AdapterCar adapterCar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);


        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapterCar = new AdapterCar(this, datasource.getAllCars());
        list.setAdapter(adapterCar);
    }

    private void initialize() {
        adapterCar = new AdapterCar();
        adapterCar.setOnComponentItemClickListener(this);

        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("خودرو");
        Misc.setFont(this, tvTitleToolbar);

        btnNewCar = (FloatingActionButton) findViewById(R.id.btnFabCreateNewCar);
        btnNewCar.setOnClickListener(this);

        list = (ListView) findViewById(R.id.listViewCar);
//        list.setAdapter(new AdapterCar(this, datasource.getAllCars()));


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tvId = (TextView) view.findViewById(R.id.tvRowCarId);
                Log.i("xxx name", tvId.getText().toString());
                Intent intent = new Intent(ActivityCar.this, ActivityRegisterCar.class);
                intent.putExtra("car_id", tvId.getText().toString());

                startActivity(intent);

            }
        });

    }

    public void DeleteCarRequest(final Context context, final String car_id) {
        String URL = Misc.Server_Url() + "delete_car.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();

                        datasource.deleteCar(car_id);
                        adapterCar = new AdapterCar(ActivityCar.this, datasource.getAllCars());
                        list.setAdapter(adapterCar);

                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        Misc.setDialog(context, "خطا", "دوباره تلاش کنید", 'e');
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("car_id", car_id);
                return params;
            }

        };
        RetryPolicy policy = new DefaultRetryPolicy(5 * 1000, 2, 2);
        sr.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(sr);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFabCreateNewCar:
                startActivity(new Intent(ActivityCar.this, ActivityRegisterCar.class));

                break;
        }
    }

    @Override
    public void Occured(int car_id) {
        DeleteCarRequest(ActivityCar.this, String.valueOf(car_id));
    }
}
