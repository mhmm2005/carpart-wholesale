package com.mansouri.wholesalecarpart.adaptor;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.R;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Car;

import java.util.List;


public class AdapterCar extends BaseAdapter {

    private static LayoutInflater inflater = null;
    DATASource datasource;
    private Activity activity;
    private List<Car> data;
    ComponentClickListener componentClickListener;

    public  AdapterCar(){}

    public AdapterCar(Activity a, List<Car> d) {
        activity = a;
        data = d;
        datasource = new DATASource(activity);
//        datasource.open();
        componentClickListener = (ComponentClickListener) a;

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.row_car, null);
        }

        TextView tvCarId = (TextView) vi.findViewById(R.id.tvRowCarId);
        TextView tvCarName = (TextView) vi.findViewById(R.id.tvRowCar);
        ImageView imgDelete = (ImageView) vi.findViewById(R.id.imgRowCarDelete);

        Misc.setFont(activity, tvCarName, 16, true);

        final Car car = data.get(position);

        tvCarId.setText(String.valueOf(car.getId()));
        tvCarName.setText(car.getName());

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                componentClickListener.Occured(car.getId());
            }
        });

        return vi;
    }
    public void setOnComponentItemClickListener(ComponentClickListener componentClickListener) {
        this.componentClickListener = componentClickListener;
    }

    public interface ComponentClickListener {
        void Occured(int car_id);
    }
}