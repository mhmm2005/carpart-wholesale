package com.mansouri.wholesalecarpart.adaptor;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.R;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Product;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;


public class AdapterProduct extends BaseAdapter {

    private static LayoutInflater inflater = null;
    DATASource datasource;
    ComponentClickListener componentClickListener;
    private Activity activity;
    private List<Product> data;

    public AdapterProduct() {
    }

    public AdapterProduct(Activity a, List<Product> d, int category_id) {
        activity = a;
        data = d;
        datasource = new DATASource(activity);
        componentClickListener = (ComponentClickListener) a;

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.row_product, null);
        }

        TextView tvProductId = (TextView) vi.findViewById(R.id.tvRowProductId);
        TextView tvProductName = (TextView) vi.findViewById(R.id.tvRowProductName);
        TextView tvProductPrice = (TextView) vi.findViewById(R.id.tvRowProductPrice);
        TextView tvProductBrand = (TextView) vi.findViewById(R.id.tvRowProductBrand);
        TextView tvProductShareCar = (TextView) vi.findViewById(R.id.tvRowProductShareCar);
        ImageView imgProductDelete = (ImageView) vi.findViewById(R.id.imgRowProductDelete);
        ImageView imgProductPhoto = (ImageView) vi.findViewById(R.id.imgRowProduct);

        Misc.setFont(activity, tvProductName, 16, true);
        Misc.setFont(activity, tvProductPrice, 14, false);
        Misc.setFont(activity, tvProductBrand, 12, false);
        Misc.setFont(activity, tvProductShareCar, 12, false);

        final Product product = data.get(position);

        tvProductId.setText(String.valueOf(product.getId()));
        tvProductName.setText("نام کالا: " + product.getName());
        tvProductPrice.setText("قیمت کالا: " + NumberFormat.getNumberInstance(Locale.US).format(product.getPrice())+" ریال ");
        tvProductShareCar.setText("مورد استفاده در خودروهای: "+ product.getShare_car());

        tvProductBrand.setText("شرکت سازنده: " + datasource.findBrandById(product.getBrand_id()).getName());

        Log.i("xxx brand", datasource.findBrandById(product.getBrand_id()).getName());


//        String root = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_PICTURES + "/carpart/" + product.getId() + ".png";

//        Random rnd = new Random();
        Picasso.with(activity)
                .load(Misc.Server_Url() + "image/p" + product.getPhoto() + ".png?random=" + Misc.getLastUpdateProduct(activity))
                .noFade()
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(imgProductPhoto);

        imgProductDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                componentClickListener.Occured(product.getId());

            }
        });

        return vi;
    }

    public void setOnComponentItemClickListener(ComponentClickListener componentClickListener) {
        this.componentClickListener = componentClickListener;
    }

    public interface ComponentClickListener {
        void Occured(int product_id);
    }

}