package com.mansouri.wholesalecarpart;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AndroidMultiPartEntity;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Product;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ActivityRegisterProduct extends AppCompatActivity implements View.OnClickListener {

    final int CAMERA_CAPTURE = 1;
    final int PIC_CROP = 2;
    long totalSize = 0;
    EditText etProductName, etProductPrice, etProductGaranty, etProductDescription, etProductBoxSize;
    TextView tvProductCommonCar, tvProductCommonCarTitle, tvProductBrand;
    Button btnRegisterProduct, btnSetProductPhoto;
    ImageView imgCarSelectPlus, imgBrandSelectPlus;
    ImageView imgProductPhoto;
    DATASource datasource;
    Product product = new Product();
    String product_id = "", category_id = "";
    LinearLayout linRegisterProduct;
    private Uri picUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_product);


        initialize();
    }

    private void initialize() {
        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("ثبت کالا");
        Misc.setFont(this, tvTitleToolbar);

        etProductName = (EditText) findViewById(R.id.etProductName);
        etProductPrice = (EditText) findViewById(R.id.etProductPrice);
        tvProductBrand = (TextView) findViewById(R.id.tvProductBrand);
        etProductGaranty = (EditText) findViewById(R.id.etProductGaranty);
        etProductDescription = (EditText) findViewById(R.id.etProductDescription);
        etProductBoxSize = (EditText) findViewById(R.id.etProductBoxSize);

        imgCarSelectPlus = (ImageView) findViewById(R.id.imgCarSelectPlus);
        imgBrandSelectPlus = (ImageView) findViewById(R.id.imgBrandSelectPlus);
        imgProductPhoto = (ImageView) findViewById(R.id.imgProductPhoto);

        tvProductCommonCar = (TextView) findViewById(R.id.tvProductCommonCar);
        tvProductCommonCarTitle = (TextView) findViewById(R.id.tvProductCommonCarTitle);

        btnRegisterProduct = (Button) findViewById(R.id.btnRegisterProduct);
        btnSetProductPhoto = (Button) findViewById(R.id.btnSetProductPhoto);

        linRegisterProduct = (LinearLayout) findViewById(R.id.linRegisterProduct);

        Misc.setFont(this, etProductName,25,false);
        Misc.setFont(this, etProductPrice,25,false);
        Misc.setFont(this, tvProductBrand,25,false);
        Misc.setFont(this, etProductGaranty,25,false);
        Misc.setFont(this, etProductDescription,25,false);
        Misc.setFont(this, etProductBoxSize,25,false);
        Misc.setFont(this, btnRegisterProduct,18,true);
        Misc.setFont(this, btnSetProductPhoto,25,false);
        Misc.setFont(this, tvProductCommonCar,25,false);
        Misc.setFont(this, tvProductCommonCarTitle,18,false);

        btnRegisterProduct.setOnClickListener(this);
        btnSetProductPhoto.setOnClickListener(this);
        imgCarSelectPlus.setOnClickListener(this);
        imgBrandSelectPlus.setOnClickListener(this);

        try {
            category_id = getIntent().getExtras().getString("category_id");
            Log.i("xxx category id", category_id);
        } catch (Exception e) {

        }

        try {
            product_id = getIntent().getExtras().getString("product_id");
            Log.i("xxx product_id", product_id);

            if (!product_id.equals("-1")) {
                product = datasource.findProductById(product_id);
                etProductName.setText(product.getName());
                etProductPrice.setText(String.valueOf(product.getPrice()));
                tvProductBrand.setText(datasource.findBrandById(product.getBrand_id()).getName());
                etProductGaranty.setText(product.getGaranty());
                etProductDescription.setText(product.getDescription());
                etProductBoxSize.setText(product.getBox_size());
                tvProductCommonCar.setText(product.getShare_car());

//                String root = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_PICTURES + "/carpart/" + product.getId() + ".png";
//                Log.i("xxx image", Misc.Server_Url() + "image/p" + product.getPhoto() + ".png");
//                Random rnd = new Random();
                Picasso.with(this)
                        .load(Misc.Server_Url() + "image/p" + product.getPhoto() + ".png?random=" + Misc.getLastUpdateProduct(this))
                        .noFade()
                        .error(R.drawable.placeholder)
                        .placeholder(R.drawable.placeholder)
                        .fit()
                        .into(imgProductPhoto);

                btnSetProductPhoto.setEnabled(true);
            } else {
                btnSetProductPhoto.setEnabled(false);
                imgProductPhoto.setVisibility(View.GONE);
            }


        } catch (Exception e) {

        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegisterProduct:

                if (isProductEditTextEmpty()) {
                    RelativeLayout rel = (RelativeLayout) findViewById(R.id.relp);
                    Misc.snack(ActivityRegisterProduct.this, rel, "لطفا تمامی فیلد ها را پر نمایید");

                } else {

                    product.setId(Integer.parseInt(product_id));
                    product.setCategory_id(Integer.parseInt(category_id));
                    product.setName(etProductName.getText().toString());
                    product.setPrice(Integer.parseInt((etProductPrice.getText().toString())));
                    product.setBrand_id(datasource.findBrandIdByName(tvProductBrand.getText().toString()));
                    product.setGaranty(etProductGaranty.getText().toString());
                    product.setDescription(etProductDescription.getText().toString());
                    product.setBox_size("1");
                    product.setActive(1);
                    product.setShare_car(tvProductCommonCar.getText().toString());

                    registerProductRequest(ActivityRegisterProduct.this, product);
                }

                break;
            case R.id.imgCarSelectPlus:
                Intent in = new Intent(ActivityRegisterProduct.this, ActivitySelectCar.class);
                in.putExtra("cars", tvProductCommonCar.getText().toString());
                startActivityForResult(in, 200);
                break;
            case R.id.imgBrandSelectPlus:
                Intent intent = new Intent(ActivityRegisterProduct.this, ActivitySelectBrand.class);
                startActivityForResult(intent, 100);
                break;

            case R.id.btnSetProductPhoto:
                Crop.pickImage(ActivityRegisterProduct.this);
                break;
        }
    }

    private boolean isProductEditTextEmpty() {
        if (
                etProductPrice.getText().toString().trim() == "" ||
                        etProductDescription.getText().toString().trim().equals("") ||
                        etProductGaranty.getText().toString().trim().equals("") ||
                        etProductPrice.getText().toString().trim().equals("") ||
                        etProductName.getText().toString().trim().equals("") ||
                        tvProductBrand.getText().toString().trim().equals("") ||
                        tvProductCommonCar.getText().toString().trim().equals("")
                ) {
            return true;

        } else {
            return false;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == 1000) {
            tvProductBrand.setText(data.getExtras().getString("brand"));
        }

        if (requestCode == 200 && resultCode == 2000) {
            tvProductCommonCar.setText(data.getExtras().getString("cars"));
        }


        if (resultCode == RESULT_OK) {
            //user is returning from capturing an image using the camera
            if (requestCode == CAMERA_CAPTURE) {
                //get the Uri for the captured image
                picUri = data.getData();
                //carry out the crop operation
                performCrop();
            } else if (requestCode == PIC_CROP) {
                //user is returning from cropping the image
                //get the returned data
                Bundle extras = data.getExtras();
                //get the cropped bitmap
                Bitmap thePic = extras.getParcelable("data");
//                imgProductPhoto.setImageBitmap(thePic);
                imgProductPhoto.setImageBitmap(thePic);

                saveImage();
            } else if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
                beginCrop(data.getData());
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }

    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            imgProductPhoto.setImageURI(Crop.getOutput(result));
            saveImage();
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void performCrop() {
        //take care of exceptions
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        //respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            //display an error message
//            String errorMessage = "Whoops - your device doesn't support the crop action!";
//            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
//            toast.show();
        }
    }

    private void saveImage() {
        imgProductPhoto.setDrawingCacheEnabled(true);
        imgProductPhoto.buildDrawingCache();
        Bitmap bm = imgProductPhoto.getDrawingCache();

        Random rnd = new Random();
        Misc.setLastUpdateProduct(ActivityRegisterProduct.this, rnd.nextLong());

        String root = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_PICTURES;
        Log.i("xxx root", root);
        File myDir = new File(root + "/carpart/product/");
        myDir.mkdirs();
        File file = new File(myDir, "p" + product.getId() + ".png");
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        ActivityRegisterProduct.this.sendBroadcast(mediaScanIntent);

//        progressUpload.setVisibility(View.VISIBLE);
        new UploadFileToServer().execute();
    }

    public void registerProductRequest(final Context context, final Product product) {
        String URL = Misc.Server_Url() + "register_product.php";
        Misc.showDialogProgress(context);
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    if (obj.getInt("success") == 1) {
                        Misc.dismissDialogProgress();

                        String product_id = obj.getString("product_id");
                        product.setId(Integer.parseInt(product_id));
                        product.setPhoto(Integer.parseInt(product_id));
                        datasource.saveOrUpdateProduct(product);

                        btnSetProductPhoto.setEnabled(true);
                        btnRegisterProduct.setEnabled(false);
                        imgProductPhoto.setVisibility(View.VISIBLE);

                        Misc.setDialog(context, "پیام سیستم", "اطلاعات با موفقیت ثبت گردید", 's');

//                        finish();

                    } else if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        Misc.dismissDialogProgress();
                        Misc.setDialog(context, "خطا", "اطلاعات اشتباه", 'e');
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());
                Misc.dismissDialogProgress();
                Misc.setDialog(context, "خطا", "اشکال در ارتباط با سرور", 'e');
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(product.getId()));
                params.put("name", product.getName());
                params.put("price", String.valueOf(product.getPrice()));
                params.put("brand_id", String.valueOf(product.getBrand_id()));
                params.put("garanty", product.getGaranty());
                params.put("description", product.getDescription());
                params.put("box_size", product.getBox_size());
                params.put("sharecar", product.getShare_car());
                params.put("active", String.valueOf(product.getActive()));
                params.put("category_id", String.valueOf(product.getCategory_id()));
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }


    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            Misc.showDialogProgress(ActivityRegisterProduct.this);

//            progressUpload.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // updating progress bar value
//            progressUpload.setProgress(progress[0]);

        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Misc.Server_Url() + "upload.php");

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });
                String root = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_PICTURES;
                String filePath = root + "/carpart/product/p" + product.getId() + ".png";
                Log.i("xxx path", filePath);
                File sourceFile = new File(filePath);

                // Adding file data to http body
                entity.addPart("image", new FileBody(sourceFile));

                // Extra parameters if you want to pass to server
//                entity.addPart("website",new StringBody("www.androidhive.info"));
//                entity.addPart("email", new StringBody("abc@gmail.com"));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {


            super.onPostExecute(result);
            Misc.dismissDialogProgress();

//            finish();
        }

    }

}
