package com.mansouri.wholesalecarpart.services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mansouri.wholesalecarpart.ActivityLogin;
import com.mansouri.wholesalecarpart.R;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.AppController;
import com.mansouri.wholesalecarpart.library.Misc;
import com.mansouri.wholesalecarpart.model.Order;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hossein on 12/19/2015.
 */
public class checkForNewOrder extends Service {
    DATASource datasource;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        datasource = new DATASource(getApplicationContext());
        getAllOrdersRequest(String.valueOf(Misc.getOrderLastUpdate(getApplicationContext())));

        stopSelf();

        return START_NOT_STICKY;
    }

    private void Notify(int showNotify) {
        if (showNotify == 1) {
            Intent i = new Intent(this, ActivityLogin.class);
            PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), i, 0);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Notification n = new Notification.Builder(this)
                    .setTicker("سفارش جدیدی دریافت شد")
                    .setContentTitle("کارپارت")
                    .setContentText("سفارش جدیدی دریافت شد")
                    .setSmallIcon(R.drawable.rth)
                    .setAutoCancel(true)
                    .setVibrate(new long[]{1000, 1000, 1000})
                    .setSound(alarmSound)
                    .setLights(Color.YELLOW, 1500, 1000)
                    .setContentIntent(pIntent).getNotification();

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(0, n);
        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        // I want to restart this service again in 1/2 hour
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.set(
                alarm.RTC_WAKEUP,
                System.currentTimeMillis() + (1000 * 60 * 30),
                PendingIntent.getService(this, 0, new Intent(this, checkForNewOrder.class), 0)
        );
    }


    public void getAllOrdersRequest(final String lastUpdate) {
        String URL = Misc.Server_Url() + "get_all_orders.php";
        final StringRequest sr = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("xxx getAllOrdersRequest", response.toString());
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    JSONArray jsonArray = obj.getJSONArray("orders");

                    if (obj.getInt("success") == 1) {

                        // amaliate zakhire orders dar DB ba maghadire ersal shode az json samte server
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonRow = jsonArray.getJSONObject(i);

                            Order order = new Order();
                            order.setId(jsonRow.getInt("id"));
                            order.setUser_id(jsonRow.getInt("user_id"));
                            order.setOrder_time(jsonRow.getLong("order_time"));
                            order.setDescription(jsonRow.getString("description"));
                            order.setAccepted(jsonRow.getInt("accepted"));
                            order.setMessage(jsonRow.getString("message"));
                            order.setOrder(jsonRow.getString("orders"));

                            if (datasource.findUserById(String.valueOf(order.getUser_id())).getActive() == 1) {
                                datasource.createOrUpdateOrder(order);
                            }

                        }


                        // namayesh Notify
                        Log.i("xxx sefaresh jadid", "ccc");
                        Notify(1);

                        // set shodane meghdare lastupdate dar preference
                        long lastUpdate = obj.getLong("last_update");
                        Misc.setOrderLastUpdate(getApplicationContext(), lastUpdate);


                    }

                    if (obj.getInt("success") == 0) {
                        Log.i("xxx ", "success=0");
                        // namayesh dar listview

                        long lastUpdate = obj.getLong("last_update");
                        Misc.setOrderLastUpdate(getApplicationContext(), lastUpdate);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("xxx err", error.toString());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("last_update", lastUpdate);
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr);
    }


}
