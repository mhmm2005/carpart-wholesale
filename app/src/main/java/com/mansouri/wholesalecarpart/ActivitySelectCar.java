package com.mansouri.wholesalecarpart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mansouri.wholesalecarpart.adaptor.AdapterSelectCar;
import com.mansouri.wholesalecarpart.database.DATASource;
import com.mansouri.wholesalecarpart.library.Misc;

public class ActivitySelectCar extends AppCompatActivity {

    ListView list;
    DATASource datasource;
    Button btnSelectCars;
    AdapterSelectCar adapterSelectCar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_car);

        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();

        String cars[] = getIntent().getExtras().getString("cars").split(",");

        adapterSelectCar = new AdapterSelectCar(this, datasource.getAllCars(),cars);
        list.setAdapter(adapterSelectCar);
    }

    private void initialize() {

        datasource = new DATASource(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvToolbar);
        tvTitleToolbar.setText("انتخاب ماشین");
        Misc.setFont(this, tvTitleToolbar);

        list = (ListView) findViewById(R.id.listViewSelectCar);
        btnSelectCars = (Button) findViewById(R.id.btnSelectCarSave);
        Misc.setFont(this,btnSelectCars);

        btnSelectCars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("cars", adapterSelectCar.getSelectedCarNames());
                setResult(2000, intent);
                finish();
            }
        });

    }

}
