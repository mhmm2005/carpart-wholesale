package com.mansouri.wholesalecarpart.model;

/**
 * Created by Hossein on 2/22/2016.
 */
public class Brand {
    int id;
    String name;

    public Brand() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
