package com.mansouri.wholesalecarpart.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Hossein on 12/19/2015.
 */
public class AutoStartBroadCast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, checkForNewOrder.class));
    }

}
